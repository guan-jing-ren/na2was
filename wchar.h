#pragma once

template <typename... Args> inline constexpr void fgetwc(Args &&...);
template <typename... Args> inline constexpr void fgetws(Args &&...);
template <typename... Args> inline constexpr void fputwc(Args &&...);
template <typename... Args> inline constexpr void fputws(Args &&...);
template <typename... Args> inline constexpr void frewind(Args &&...);
template <typename... Args> inline constexpr void fwide(Args &&...);
template <typename... Args> inline constexpr void fwprintf(Args &&...);
template <typename... Args> inline constexpr void fwrite(Args &&...);
template <typename... Args> inline constexpr void fwscanf(Args &&...);
template <typename... Args> inline constexpr void getwc(Args &&...);
template <typename... Args> inline constexpr void getwchar(Args &&...);
template <typename... Args> inline constexpr void putwc(Args &&...);
template <typename... Args> inline constexpr void putwchar(Args &&...);
template <typename... Args> inline constexpr void rewind(Args &&...);
template <typename... Args> inline constexpr void swprintf(Args &&...);
template <typename... Args> inline constexpr void swscanf(Args &&...);
template <typename... Args> inline constexpr void ungetwc(Args &&...);
template <typename... Args> inline constexpr void vfwprintf(Args &&...);
template <typename... Args> inline constexpr void vfwscanf(Args &&...);
template <typename... Args> inline constexpr void vswprintf(Args &&...);
template <typename... Args> inline constexpr void vswscanf(Args &&...);
template <typename... Args> inline constexpr void vwprintf(Args &&...);
template <typename... Args> inline constexpr void vwscanf(Args &&...);
template <typename... Args> inline constexpr void wprintf(Args &&...);
template <typename... Args> inline constexpr void wscanf(Args &&...);

template <typename... Args> inline constexpr void wcsftime(Args &&...);
template <typename... Args> inline constexpr void wcstod(Args &&...);
template <typename... Args> inline constexpr void wcstof(Args &&...);
template <typename... Args> inline constexpr void wcstol(Args &&...);
template <typename... Args> inline constexpr void wcstold(Args &&...);
template <typename... Args> inline constexpr void wcstoll(Args &&...);
template <typename... Args> inline constexpr void wcstoul(Args &&...);
template <typename... Args> inline constexpr void wcstoull(Args &&...);

template <typename... ArgTypes> inline constexpr void wcscat(ArgTypes &&...);
template <typename... ArgTypes> inline constexpr void wcscmp(ArgTypes &&...);
template <typename... ArgTypes> inline constexpr void wcscoll(ArgTypes &&...);
template <typename... ArgTypes> inline constexpr void wcscpy(ArgTypes &&...);
template <typename... ArgTypes> inline constexpr void wcscspn(ArgTypes &&...);
template <typename... ArgTypes> inline constexpr void wcserror(ArgTypes &&...);
extern "C" std::size_t wcslen(const char *);
template <typename... ArgTypes> inline constexpr void wcsncat(ArgTypes &&...);
template <typename... ArgTypes> inline constexpr void wcsncmp(ArgTypes &&...);
template <typename... ArgTypes> inline constexpr void wcsncpy(ArgTypes &&...);
template <typename... ArgTypes> inline constexpr void wcsspn(ArgTypes &&...);
template <typename... ArgTypes> const char *wcswcs(ArgTypes &&...);
template <typename... ArgTypes> inline constexpr void wcstok(ArgTypes &&...);
template <typename... ArgTypes> inline constexpr void wcsxfrm(ArgTypes &&...);

template <typename... ArgTypes>
inline constexpr const wchar_t *wcschr(ArgTypes &&...);
template <typename... ArgTypes>
inline constexpr const wchar_t *wcspbrk(ArgTypes &&...);
template <typename... ArgTypes>
inline constexpr const wchar_t *wcsrchr(ArgTypes &&...);
template <typename... ArgTypes>
inline constexpr const wchar_t *wcsstr(ArgTypes &&...);
template <typename... ArgTypes>
inline constexpr const wchar_t *wmemchr(ArgTypes &&...);

template <typename... Args> inline constexpr void btowc(Args &&...);
template <typename... Args> inline constexpr void mbrlen(Args &&...);
template <typename... Args> inline constexpr void mbsinit(Args &&...);
template <typename... Args> inline constexpr void mbrtowc(Args &&...);
template <typename... Args> inline constexpr void mbsrtowcs(Args &&...);
template <typename... Args> inline constexpr void wctob(Args &&...);
template <typename... Args> inline constexpr void wcrtomb(Args &&...);
template <typename... Args> inline constexpr void wcsrtombs(Args &&...);

template <typename... ArgTypes> inline constexpr void wmemcmp(ArgTypes &&...);
extern "C" void *wmemcpy(void *dest, const void *src, std::size_t count);
extern "C" void *wmemmove(void *dest, const void *src, std::size_t count);
extern "C" void *wmemset(void *dest, int c, std::size_t count);

class mbstate_t;