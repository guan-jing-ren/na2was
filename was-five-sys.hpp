#pragma once

#include "was-five-culture.hpp"
#include "was-five-person.hpp"
#include "was-five-post.hpp"

#include <variant.hpp>

#include <unordered_set>

using day_t = std::chrono::time_point<
    std::chrono::system_clock,
    std::chrono::duration<
        std::chrono::hours::rep,
        std::ratio_multiply<std::chrono::hours::period, std::ratio<24>>>>;

struct Advert {
  struct Id {
  } id;
  struct Country {
  } country;
  struct Image {
  } image;
  struct Link {
  } link;
  int64_t renew_count;
  int64_t view_count; // Afer 5 seconds.
  struct Discount {
  } discount;
  struct Keyword {};
  std::vector<Keyword> keywords;
};

struct Bid {
  enum Tier { PSA, GLOBAL, COUNTRY, SPONSOR } tier;
  enum Position {
    CENTRE,
    CENTRE_LEFT,
    CENTRE_RIGHT,
    FAR_LEFT,
    FAR_RIGHT
  } position;
  int64_t amount, counterbid;
  day_t day;
  Advert advert;
};

bool operator==(const Bid &, const Bid &);

struct Flagged {
  enum Reason { ILLEGAL, HARASSMENT } reason;
  Person reporter;
  std::string report;
  std::string response;
  std::string resolution;

  gut::variant<Advert, Person, Post, Culture> entity;
};

struct Shared {
  std::vector<Person> reviewers;
  Post post;
  std::vector<Person> to, not_to; // shared to specific individuals.
  std::unordered_set<Person::Relationship> with,
      not_with;                          // shared with group of people.
  std::vector<Culture> among, not_among; // shared among participants.
};

struct Introduction {
  Person to, from;
  Culture invite;
  Person by; // Mutual contact.
};

struct Session {
  struct Id {
  } id;
  Person person;
  enum Portal { FIVE, THREE };
  std::vector<Post> drafts, posted, shared;
};

// email banned -> PersonNotAuthorized
// email and nickname exists -> PersonAlreadyExists
RemoteCode<SuccessfulOperation::PersonRegistered,
           ErrorNotAuthorized::PersonNotAuthorized,
           ErrorAlreadyExists::PersonAlreadyExists>
handle(gut::constant<Operation::SignUp>, const Person::Email &email,
       const Person::Nickname &nickname);

RemoteCode<SuccessfulOperation::PersonDeregistered>
handle(gut::constant<Operation::SignDown>, const Session &session);

// bid not in session.person.bid -> SessionNotAuthorized
// advert banned -> AdvertNotAuthorized
SessionCode<SuccessfulOperation::BidPlaced,
            ErrorAlreadyExists::AdvertAlreadyExists,
            ErrorNotAuthorized::AdvertNotAuthorized,
            ErrorAlreadyExists::BidAlreadyExists>
handle(gut::constant<Operation::PlaceBid>, const Session &session,
       Bid::Tier tier, Bid::Position position, int64_t amount, day_t day,
       const Advert &ad);
// bid not in session.person.bid -> SessionNotAuthorized
SessionCode<SuccessfulOperation::BidDropped, ErrorNotExists::BidNotExists>
handle(gut::constant<Operation::DropBid>, const Session &session,
       const Bid &bid);
// bid not in session.person.bid -> SessionNotAuthorized
// advert banned -> AdvertNotAuthorized
SessionCode<SuccessfulOperation::BidModified,
            ErrorNotAuthorized::AdvertNotAuthorized,
            ErrorNotExists::BidNotExists, ErrorAlreadyExists::BidAlreadyExists>
handle(gut::constant<Operation::RenewBid>, const Session &session,
       Bid::Tier tier, Bid::Position position, int64_t amount, day_t day,
       const Advert &ad);
// bid not in session.person.bid -> SessionNotAuthorized
// advert banned -> AdvertNotAuthorized
// bid not in session.person.bid -> SessionNotAuthorized
SessionCode<SuccessfulOperation::BidModified,
            ErrorNotAuthorized::AdvertNotAuthorized,
            ErrorNotExists::BidNotExists, ErrorAlreadyExists::BidAlreadyExists>
handle(gut::constant<Operation::AcceptCounterBid>, const Session &session,
       const Bid &bid);
SessionCode<SuccessfulOperation::BidModified, ErrorNotExists::BidNotExists>
handle(gut::constant<Operation::RejectCounterBid>, const Session &session,
       const Bid &bid);

RemoteCode<SuccessfulOperation::SessionStarted, ErrorNotExists::PersonNotExists,
           ErrorNotAuthorized::PersonNotAuthorized>
handle(gut::constant<Operation::Login>, const Person::Email &email,
       const Person::Credentials &password);
// Unilateral, always successful
RemoteCode<SuccessfulOperation::SessionEnded>
handle(gut::constant<Operation::Logout>, const Session &session);

// Unilateral
// session.person reported advert -> ReportAlreadyExists
SessionCode<SuccessfulOperation::AdvertReported,
            ErrorNotExists::AdvertNotExists>
handle(gut::constant<Operation::Report>, const Session &session,
       const Advert &advert, Flagged::Reason, const std::string &report);
// Unilateral
// session.person reported person -> ReportAlreadyExists
SessionCode<SuccessfulOperation::PersonReported,
            ErrorNotExists::PersonNotExists>
handle(gut::constant<Operation::Report>, const Session &session,
       const Person &person, Flagged::Reason, const std::string &report);
// Unilateral
// session.person reported culture -> ReportAlreadyExists
SessionCode<SuccessfulOperation::CultureReported,
            ErrorNotExists::CultureNotExists>
handle(gut::constant<Operation::Report>, const Session &session,
       const Culture &culture, Flagged::Reason, const std::string &report);
// Unilateral
// session.person reported post -> ReportAlreadyExists
SessionCode<SuccessfulOperation::PostReported, ErrorNotExists::PostNotExists>
handle(gut::constant<Operation::Report>, const Session &session,
       const Post &post, Flagged::Reason, const std::string &report);
