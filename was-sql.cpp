#include "was-sql.hpp"

#include <chrono>
#if !defined(NDEBUG)
#include <iostream>
#endif

using namespace gut;
using namespace std;

template <auto SQLiteFunc, bool Timed, typename... Args>
variant<ntuple<int, int>, vector<result_row>> SQLite::call(Args... args) const {

  gut::optional<vector<result_row>> results;
step:

#if !defined(NDEBUG)
  auto start = chrono::high_resolution_clock::now();
#endif

  auto rc = SQLiteFunc(args...);

#if !defined(NDEBUG)
  if constexpr (Timed) {
    auto time = chrono::high_resolution_clock::now() - start;
    clog << "Query execution time: "
         << chrono::duration_cast<chrono::milliseconds>(time).count() << '\n';
  }
#endif

  switch (rc) {
  default: {
    cerr << "Return code: " << rc << ", " << sqlite3_errmsg(m_resource) << '\n';
    auto ec = sqlite3_extended_errcode(m_resource);
    cerr << "Extended error: " << sqlite3_errstr(ec) << '\n';
    return ntuple{rc, ec};
  }
  case SQLITE_ROW:
    if constexpr (type_v<decltype(SQLiteFunc)>.remove_pointer() ==
                  type_v<decltype(sqlite3_step)>)
      if constexpr (SQLiteFunc == sqlite3_step) {
        if (!results)
          results = vector<result_row>{};
        results.value().push_back({});
        auto &result = results.value().back();
        for (int i = 0, count = sqlite3_column_count(args...); i < count; ++i) {
          [[maybe_unused]] auto column_name = sqlite3_column_name(args..., i);

#if !defined(NDEBUG)
          clog << "Query column name: " << column_name << '\n';
#endif

          switch (sqlite3_column_type(args..., i)) {
          default:
            break;
          case SQLITE_INTEGER:
            result[column_name] = sqlite3_column_int64(args..., i);
            break;
          case SQLITE_FLOAT:
            result[column_name] = sqlite3_column_double(args..., i);
            break;
          case SQLITE_TEXT: {
            auto *text = sqlite3_column_text(args..., i);
            auto length = sqlite3_column_bytes(args..., i);
            string value;
            value.reserve(length);
            copy(text, text + length, back_inserter(value));
#if !defined(NDEBUG)
            clog << "SQLITE_TEXT value: " << value << '\n';
#endif
            result[column_name] = move(value);
          } break;
          case SQLITE_BLOB: {
            auto *blob = static_cast<const unsigned char *>(
                sqlite3_column_blob(args..., i));
            auto length = sqlite3_column_bytes(args..., i);
            vector<unsigned char> value;
            value.reserve(length);
            copy(blob, blob + length, back_inserter(value));
            result[column_name] = move(value);
          } break;
          }
        }
        goto step;
      }
    cerr << "SQLITE_ROW returned not from sqlite3_step\n";
  case SQLITE_OK:
  case SQLITE_DONE:
    break;
  }

  if (results) {
#if !defined(NDEBUG)
    clog << "Number of rows returned: " << results.value().size() << '\n';
    for (auto &&row : results.value())
      for (auto &&column : row)
        clog << "Column name: " << column.first << '\n';
#endif

    return move(results.value());
  }

  return ntuple{rc, 0};
}

SQLite::InternalString::InternalString(char *s) { m_resource = s; }
SQLite::InternalString::operator const char *() const {
  return m_resource ? m_resource : "";
}

SQLite::PreparedStatement::PreparedStatement(const SQLite &db, string_view sql)
    : m_db(db) {
  m_db.get().call<sqlite3_prepare_v3>(m_db.get().m_resource, sql.data(),
                                      sql.size(), SQLITE_PREPARE_PERSISTENT,
                                      &m_resource, nullptr);
}

gut::optional<vector<result_row>> SQLite::PreparedStatement::operator()(
    unordered_map<string, column_value> parameters) const {
  if (!valid())
    return {};

  for (auto &&[name, value] : parameters) {
    auto index = sqlite3_bind_parameter_index(m_resource, name.data());
    value.each([this, index](auto &&value) {
      constexpr type value_type = type_v<decltype(value)>.remove_cvref();
      if constexpr (value_type == type_v<int64_t>)
        m_db.get().call<sqlite3_bind_int64>(m_resource, index, value);
      else if constexpr (value_type == type_v<double>)
        m_db.get().call<sqlite3_bind_double>(m_resource, index, value);
      else if constexpr (value_type == type_v<string>)
        m_db.get().call<sqlite3_bind_text64>(m_resource, index, value.c_str(),
                                             value.length(), SQLITE_TRANSIENT,
                                             SQLITE_UTF8);
      else if constexpr (value_type == type_v<vector<unsigned char>>)
        m_db.get().call<sqlite3_bind_blob64>(m_resource, index, value.data(),
                                             value.size(), SQLITE_TRANSIENT);
    });
  }

#if !defined(NDEBUG)
  clog << InternalString{sqlite3_expanded_sql(m_resource)} << '\n';
#endif

  auto result = m_db.get().call<sqlite3_step, true>(m_resource);
  m_db.get().call<sqlite3_clear_bindings>(m_resource);
  m_db.get().call<sqlite3_reset>(m_resource);
  return result.reduce([](auto &&results) {
    constexpr type result_type = type_v<decltype(results)>.remove_cvref();
    if constexpr (result_type == type_v<vector<result_row>>) {
      return move(results);
    }
    return vector<result_row>{};
  });
}

SQLite::Transaction::Transaction(const SQLite &db,
                                 const PreparedStatement &rollback,
                                 const PreparedStatement &end)
    : m_db(db), m_rollback(rollback), m_commit(end) {}

SQLite::Transaction::~Transaction() {
  if (auto ec = sqlite3_errcode(m_db.get().m_resource);
      (ec != SQLITE_OK && ec != SQLITE_DONE && ec != SQLITE_ROW) ||
      current_exception())
    m_rollback.get()();
  else
    m_commit.get()();
}

SQLite::SQLite() {
  [[maybe_unused]] const static auto init = sqlite3_initialize();
}

SQLite::SQLite(const filesystem::path &dbfile, OpenMode mode,
               NameAttributes attr)
    : SQLite() {
  call<sqlite3_open_v2>(dbfile.u8string().c_str(), &m_resource, mode | attr,
                        nullptr);

  PreparedStatement{*this, R"SQL(
PRAGMA journal_mode=WAL;
)SQL"}();

  PreparedStatement{*this, R"SQL(
PRAGMA secure_delete=ON;
)SQL"}();

  m_optimize = {*this, R"SQL(
PRAGMA optimize;
)SQL"};

  m_rollback = {*this, R"SQL(
ROLLBACK TRANSACTION;
)SQL"};

  m_begin = {*this, R"SQL(
BEGIN TRANSACTION;
)SQL"};

  m_commit = {*this, R"SQL(
COMMIT TRANSACTION;
)SQL"};
}

SQLite::~SQLite() {
  if (!valid())
    return;

  optimize();
}

void SQLite::transact(
    const std::function<void(const SQLite &)> &scope_guard) const {
  m_begin.value()();
  [[maybe_unused]] Transaction transaction{*this, m_rollback.value(),
                                           m_commit.value()};
  scope_guard(*this);
}

void SQLite::optimize() const { m_optimize.value()(); }
