#pragma once

#include "../gut/buffer.hpp"
#include "../gut/optional.hpp"
#include "../gut/thing.hpp"

#include <any>
#include <functional>

#if defined(__clang__) && __has_builtin(__builtin_wasm_memory_size)
#define IS_WASM true
#else
#define IS_WASM false
#endif

template <typename...> inline constexpr auto is_wasm = gut::constant_v<IS_WASM>;

struct JsRef {
  using callback_t = std::function<std::any(struct JsRef)>;
  using registrar_t = std::function<void(gut::ref<callback_t &>)>;

  static std::int32_t argument_size();

  JsRef(const JsRef &) = delete;
  JsRef(JsRef &&that);
  JsRef &operator=(const JsRef &) = delete;
  JsRef &operator=(JsRef &&that);
  ~JsRef();

  void release();

  template <typename... ArgTypes>
  JsRef(const char *name, const ArgTypes &... args)
      : JsRef(
            std::move(window().call_priv<JsRef>(true, name, args...).value())) {
  }

  static JsRef window();

  template <typename ReturnType, typename NameType>
  inline gut::optional<ReturnType> property(const NameType &name) const {
    volatile std::int8_t return_type = -1;
    Argument return_value = property_priv(name, &return_type);
    return handle_return<ReturnType>(return_value, return_type);
  }

  template <typename PropertyType, typename NameType>
  inline void property(const NameType &name, const PropertyType &value) const {
    property_priv(name, get_arg_value(value), get_arg_type<PropertyType>());
  }

  template <typename ReturnType>
  inline gut::optional<ReturnType> call(const char *name) const {
    return call_priv<ReturnType>(false, name);
  }

  template <typename ReturnType, typename... ArgTypes>
  inline gut::optional<ReturnType> call(const char *name,
                                        const ArgTypes &... args) const {
    return call_priv<ReturnType>(false, name, args...);
  }

private:
  friend struct JsRefCopy;
  friend struct Invoker;
  friend bool operator<(const JsRef &, const JsRef &);
  friend bool operator==(const JsRef &, const JsRef &);
  friend class std::hash<JsRef>;

  std::int32_t m_id_hi = -1;
  std::int32_t m_id_lo = -1;
  JsRef(std::int32_t) = delete;
  JsRef(std::int32_t id_hi, std::int32_t id_lo);

  union Argument {
    std::int32_t i;
    double d;
    const char *s;
    struct {
      std::int32_t hi;
      std::int32_t lo;
    } id;
    void (*f)(JsRef);
    volatile callback_t *c;

    Argument();
    Argument(const Argument &) = default;
    Argument(Argument &&) = default;
    Argument &operator=(const Argument &) = default;
    Argument &operator=(Argument &&) = default;

    Argument(std::int8_t type, const volatile Argument &that);

    Argument(std::int32_t i);
    Argument(double d);
    Argument(const char *s);
    Argument(void (*f)(JsRef));
    Argument(std::int32_t hi, std::int32_t lo);
    Argument(callback_t);
    Argument(gut::ref<callback_t &>);
  };

  template <typename ReturnType, typename... ArgTypes>
  inline gut::optional<ReturnType> call_priv(bool construct, const char *name,
                                             const ArgTypes &... args) const {
    constexpr gut::buffer argt{{get_arg_type<ArgTypes>()...}};
    gut::buffer argv{{get_arg_value(args)...}};
    volatile std::int8_t return_type = -1;
    Argument return_value = apply(construct, name, argv.data(), argt.data(),
                                  sizeof...(ArgTypes), &return_type);
    return handle_return<ReturnType>(return_value, return_type);
  }

  template <typename ReturnType>
  inline gut::optional<ReturnType> call_priv(bool construct,
                                             const char *name) const {
    volatile std::int8_t return_type = -1;
    Argument return_value =
        apply(construct, name, nullptr, nullptr, 0, &return_type);
    return handle_return<ReturnType>(return_value, return_type);
  }

  template <typename ReturnType>
  static inline gut::optional<ReturnType>
  handle_return(Argument return_value, std::int8_t return_type) {
    constexpr auto return_t = gut::type_v<ReturnType>;
    if constexpr (return_t == gut::type_v<void *>)
      return {nullptr};

    switch (return_type) {
    case 0:
      if constexpr (return_t.template is_convertible_from<int>())
        return {return_value.i};
      break;
    case 1:
      if constexpr (return_t.template is_convertible_from<double>())
        return {return_value.d};
      break;
    case 2:
      if constexpr (return_t == gut::type_v<const char *>)
        return {return_value.s};
      break;
    case 3:
      if constexpr (return_t.template is_derived_from<JsRef>())
        return ReturnType{return_value.id.hi, return_value.id.lo};
      else
        JsRef{return_value.id.hi,
              return_value.id.lo}; // Need to take ownership to prevent leak.
      break;
    };

#if IS_WASM
    abort("Return type not supported.");
#endif
    return {};
  }

  Argument apply(bool construct, const char *name,
                 const volatile Argument *arguments,
                 const volatile std::int8_t *discriminants, std::int32_t count,
                 volatile std::int8_t *return_type) const;

  Argument property_priv(int32_t index,
                         volatile std::int8_t *return_type) const;
  void property_priv(int32_t index, Argument value, std::int8_t type) const;
  Argument property_priv(const char *name,
                         volatile std::int8_t *return_type) const;
  void property_priv(const char *name, Argument value, std::int8_t type) const;

  template <typename ArgType>
  static inline constexpr std::int8_t get_arg_type() {
    auto arg_type = gut::type_v<ArgType>.remove_cvref();
    if constexpr (arg_type.is_integral()) {
      static_assert(arg_type == gut::type_v<std::int32_t>);
      return 0;
    } else if constexpr (arg_type.is_floating_point()) {
      static_assert(arg_type == gut::type_v<double>);
      return 1;
    } else if constexpr (arg_type.template is_convertible_to<const char *>())
      return 2;
    else if constexpr (arg_type.template is_derived_from<JsRef>())
      return 3;
    else if constexpr (arg_type.template is_convertible_to<void (*)(JsRef)>())
      return 4;
    else if constexpr (arg_type.template is_convertible_to<callback_t>() ||
                       arg_type == gut::type_v<gut::ref<callback_t &>>)
      return 5;
    return -1;
  }

  template <typename ArgType>
  static inline Argument get_arg_value(const ArgType &arg) {
    auto arg_type = gut::type_v<ArgType>.remove_cvref();
    if constexpr (arg_type.is_integral())
      return {arg};
    else if constexpr (arg_type.is_floating_point())
      return {arg};
    else if constexpr (arg_type.template is_convertible_to<const char *>())
      return {arg};
    else if constexpr (arg_type.template is_derived_from<JsRef>())
      return {arg.m_id_hi, arg.m_id_lo};
    else if constexpr (arg_type.template is_convertible_to<void (*)(JsRef)>())
      return {static_cast<void (*)(JsRef)>(arg)};
    else if constexpr (arg_type.template is_convertible_to<callback_t>())
      return {callback_t(arg)};
    else if constexpr (arg_type == gut::type_v<gut::ref<callback_t &>>)
      return {arg};
    (void)arg;
    return {-1};
  }
};

bool operator<(const JsRef &l, const JsRef &r);
bool operator==(const JsRef &l, const JsRef &r);

template <> class std::hash<JsRef> {
public:
  size_t operator()(const JsRef &) const;
};

struct JsRefCopy : JsRef {
  using JsRef::JsRef;
  using JsRef::operator=;

  JsRefCopy(JsRef);
  JsRefCopy(const JsRefCopy &);
  JsRefCopy(JsRefCopy &&) = default;
  JsRefCopy &operator=(const JsRefCopy &);
  JsRefCopy &operator=(JsRefCopy &&) = default;
};

struct JsString : JsRef {
  JsString();
  JsString(const char *);

private:
  friend struct JsRef;
  using JsRef::JsRef;
};

struct JsWindow : JsRef {
  JsWindow();

  JsWindow top() const;
  struct Document document() const;

  using JsRef::call;
  using JsRef::property;

private:
  friend struct JsRef;
  using JsRef::JsRef;
};
