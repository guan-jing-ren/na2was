#pragma once

#include "was-mangle.hpp"

template <typename... Reflected>
struct define_environment : mangle_environment<Reflected...> {
  using define_environment_base = mangle_environment<Reflected...>;
  using define_environment_base::define_environment_base;

  define_environment(Reflected... reflected)
      : define_environment_base(reflected...) {
    register_definitions();
  }

  template <typename NativeType> JsRef map_to_js(const NativeType &n) const {
    return std::move(
        JsWindow{}
            .top()
            .call<JsRef>("__NA2WASCreateNativeObject",
                         this->mangle(gut::type_v<NativeType>.remove_cvref())
                             .to_buffer()
                             .data(),
                         reinterpret_cast<int32_t>(&n))
            .value());
  }

private:
  template <typename Type, typename Value>
  auto extract_value(Type type, const Value &value) const {
    using type_t = typename decltype(type)::raw_type;
    if constexpr (type.is_integral() || gut::is_integer_v<type_t>)
      return static_cast<int32_t>(value);
    else if constexpr (type.is_floating_point())
      return static_cast<double>(value);
    else if constexpr (type.is_class())
      return JsRefCopy{std::move(map_to_js(value))};
  }

  template <typename Type>
  void register_definition(Type type, JsRef object) const {
    JsWindow{}.top().call<void *>("__NA2WASDefineNativeObject",
                                  this->mangle(type).to_buffer().data(),
                                  static_cast<int32_t>(type.size_of()), object);
  }

  template <typename CompoundType = void *>
  void register_definitions(gut::optional<CompoundType> compound = {}) const {
    if (compound) {
      constexpr auto type = []() {
        if constexpr (gut::is_constant_v<CompoundType>) {
          constexpr auto member_type =
              gut::type_v<decltype(CompoundType::value)>.remove_cvref();
          if constexpr (member_type.is_member_pointer())
            return member_type.remove_member_pointer();
          else
            return gut::type_v<void *>;
        } else if constexpr (gut::is_type_v<CompoundType>)
          return CompoundType{};
        else
          return gut::type_v<void *>;
      }();

      using type_t = typename decltype(type)::raw_type;
      JsRef object{"Object"};
      if constexpr (gut::is_bitfield_v<type_t>) {
        object.property("__NA2WAS::size", [this](JsRef) -> std::any {
          return extract_value(gut::type_v<gut::count_t>, type_t::count());
        });

        object.property(
            "__NA2WAS::operator[]", [this, type](JsRef args) -> std::any {
              (void)this;
              auto index = args.property<int32_t>(1).value();
              std::any rv;
              gut::make_index_list<type_t::count()>().each(
                  [this, index, type, &args, &rv](auto i) mutable {
                    if (index == i)
                      rv = this->extract_value(
                          gut::type_v<uint64_t>,
                          (*type.add_const().add_pointer().cast(
                              args.property<int32_t>(0).value()))[i]);
                  });
              return rv;
            });

        register_definition(type, std::move(object));
      } else if constexpr (gut::is_ntuple_v<type_t>) {
        type_t::member_types_v().reduce([this](auto... value_type) {
          (..., this->register_definitions(gut::optional{value_type}));
        });

        object.property("__NA2WAS::size", [this](JsRef) -> std::any {
          return extract_value(gut::type_v<gut::count_t>, type_t::count());
        });

        object.property(
            "__NA2WAS::operator[]", [this, type](JsRef args) -> std::any {
              (void)this;
              auto index = args.property<int32_t>(1).value();
              std::any rv;
              type.add_const()
                  .add_pointer()
                  .cast(args.property<int32_t>(0).value())
                  ->each([this, index, &rv, i = 0](auto &&value) mutable {
                    if (index == i++)
                      rv = this->extract_value(
                          gut::type_v<decltype(value)>.remove_cvref(), value);
                  });
              return rv;
            });

        register_definition(type, std::move(object));
      } else if constexpr (gut::is_optional_v<type_t>) {
        constexpr auto value_type = gut::type_v<typename type_t::value_type>;
        register_definitions(gut::optional{value_type});

        object.property(
            "__NA2WAS::value", [this, type](JsRef args) -> std::any {
              auto rv = type.add_const().add_pointer().cast(
                  args.property<int32_t>(0).value());
              if (!*rv)
                return nullptr;

              return extract_value(gut::type_v<typename type_t::value_type>,
                                   rv->value());
            });

        register_definition(type, std::move(object));
      } else if constexpr (gut::is_variant_v<type_t>) {
        type_t::member_types_v().reduce([this](auto... value_type) {
          (..., this->register_definitions(gut::optional{value_type}));
        });

        object.property("__NA2WAS::size", [this](JsRef) -> std::any {
          return extract_value(gut::type_v<gut::count_t>, type_t::count());
        });

        object.property(
            "__NA2WAS::operator[]", [this, type](JsRef args) -> std::any {
              (void)this;
              auto index = args.property<int32_t>(1).value();
              auto vr = type.add_const().add_pointer().cast(
                  args.property<int32_t>(0).value());
              if (index != vr->index())
                return nullptr;

              std::any rv;
              vr->each([this, &rv](auto &&value) mutable {
                rv = this->extract_value(
                    gut::type_v<decltype(value)>.remove_cvref(), value);
              });
              return rv;
            });

        register_definition(type, std::move(object));
      } else if constexpr (is_sequence_container_v<type_t> ||
                           gut::is_buffer_v<type_t>) {
        constexpr auto value_type = gut::type_v<typename type_t::value_type>;
        register_definitions(gut::optional{value_type});

        object.property("__NA2WAS::size", [this, type](JsRef args) -> std::any {
          auto value = type.add_const().add_pointer().cast(
              args.property<int32_t>(0).value());
          if constexpr (is_sequence_container_v<type_t>)
            return extract_value(gut::type_v<typename type_t::size_type>,
                                 value->size());
          else
            return extract_value(gut::type_v<gut::count_t>, value->count());
        });

        object.property(
            "__NA2WAS::iterator",
            [this, type, value_type](JsRef args) -> std::any {
              (void)this;
              auto first = type.add_const()
                               .add_pointer()
                               .cast(args.property<int32_t>(0).value())
                               ->begin();
              auto end = type.add_const()
                             .add_pointer()
                             .cast(args.property<int32_t>(0).value())
                             ->end();
              return JsRef::callback_t{[this, value_type, first,
                                        end](JsRef) mutable -> std::any {
                (void)this;
                if (first == end)
                  return {};

                JsRef object{"Object"};
                object.property("done", static_cast<int32_t>(false));
                object.property("value", extract_value(value_type, *first++));
                return JsRefCopy{std::move(object)};
              }};
            });

        object.property("__NA2WAS::operator[]",
                        [this, type, value_type](JsRef args) -> std::any {
                          (void)this;
                          auto &&rv =
                              type.add_const()
                                  .add_pointer()
                                  .cast(args.property<int32_t>(0).value())
                                  ->
                                  operator[](args.property<int32_t>(1).value());

                          return extract_value(value_type, rv);
                        });

        register_definition(type, std::move(object));
      }

      return;
    }

    this->traverse([this](auto reflect) {
      using ReflectType = typename decltype(
          gut::type_v<decltype(reflect)>.remove_cvref())::raw_type;
      if constexpr (gut::is_reflect_struct_v<ReflectType>) {
        JsRef object{"Object"};
        reflect.bases().each([this, &object, reflect](auto... base) mutable {
          if constexpr (sizeof...(base) > 0) {
            this->register_definitions(gut::optional{base...});

            (...,
             object.property(
                 this->mangle(base).to_buffer().data(),
                 [this, base, reflect](JsRef arg) -> std::any {
                   return extract_value(
                       base,
                       base.add_const().add_lvalue_reference().cast(
                           *reflect.type_v().add_const().add_pointer().cast(
                               arg.property<int32_t>(0).value())));
                 }));
          }
        });

        auto names =
            reflect.names().map([](auto name) { return name.to_buffer(); });
        auto properties = names.reduce(
            [](auto &&... names) { return gut::buffer{{names.data()...}}; });

        reflect.members().each(
            [this, &object, &properties, i = 0, reflect](auto member) mutable {
              (void)this;
              this->register_definitions(gut::optional{member});
              object.property(properties[i++],
                              [this, reflect, member](JsRef args) -> std::any {
                                return extract_value(
                                gut::type_v<decltype(member.value)>.remove_member_pointer().remove_cvref(),
                                reflect.type_v().add_const().add_pointer().cast(
                                    args.property<int32_t>(0).value())
                                        ->*(member.value));
                              });
            });

        register_definition(reflect.type_v(), std::move(object));
      }
    });
  }
};
