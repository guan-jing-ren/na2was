#pragma once

template <typename... Args> void iswalnum(Args &&...);
template <typename... Args> void iswalpha(Args &&...);
template <typename... Args> void iswblank(Args &&...);
template <typename... Args> void iswcntrl(Args &&...);
template <typename... Args> void iswctype(Args &&...);
template <typename... Args> void iswdigit(Args &&...);
template <typename... Args> void iswgraph(Args &&...);
template <typename... Args> void iswlower(Args &&...);
template <typename... Args> void iswprint(Args &&...);
template <typename... Args> void iswpunct(Args &&...);
template <typename... Args> void iswspace(Args &&...);
template <typename... Args> void iswupper(Args &&...);
template <typename... Args> void iswxdigit(Args &&...);
template <typename... Args> void towlower(Args &&...);
template <typename... Args> void towupper(Args &&...);
template <typename... Args> void towctrans(Args &&...);
template <typename... Args> void wctrans(Args &&...);
template <typename... Args> void wctype(Args &&...);

struct wctrans_t;
struct wctype_t;
