#include "was-five-connections.hpp"

using namespace std;

Connections::Connections() {
  m_pool.resize(thread::hardware_concurrency());
  generate(m_pool.begin(), m_pool.end(), [this]() {
    return thread{[this]() {
      unique_lock<mutex> lock{m_request_mx};
      while (true)
        m_request_cv.wait(lock, [this]() { return !m_requests.empty(); });
    }};
  });
}
