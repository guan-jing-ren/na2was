SHELL=/bin/bash
.SHELLFLAGS=-O extglob -c
CXX=nice -n 21 ~/clang/bin/clang++
CXXFLAGS=-std=c++17 -Os -g -gsplit-dwarf -Wall -Wextra -Wpedantic -Werror -stdlib=libc++ -fstrict-aliasing -I ../gut/ -I ../sehr-gut -I ../boost/include -L ../boost/lib -pthread -lsqlite3
WXXFLAGS=-std=c++17 -Os -Wall -Wextra -Wpedantic -Werror -I ~/clang/include/c++/v1 -I . -I ../gut/ -I ../sehr-gut -I ../boost/include\
	-D_LIBCPP_DISABLE_EXTERN_TEMPLATE -D_LIBCPP_HAS_NO_THREADS -D_LIBCPP_DISABLE_VISIBILITY_ANNOTATIONS\
	--target=wasm32 -flto -nostdlib -fno-builtin-memset -fvisibility=hidden -fvisibility-inlines-hidden -finline-functions -finline-hint-functions -fno-exceptions -fno-rtti

%.so: %.cpp %.hpp Makefile
	$(CXX) $(CXXFLAGS) -shared -fPIC $< -o $@

%.wo: %.wpp %.hpp Makefile *.h
	$(CXX) $(WXXFLAGS) -x c++ -c $< -o $@

was.wasm: $(addsuffix .wo, $(basename $(wildcard *.wpp))) $(addsuffix .hpp, $(basename $(wildcard *.wpp)))
	wasm-ld -o $@ --no-entry --strip-all --export-dynamic --allow-undefined --import-memory -error-limit=0 --lto-O3 -O3 --gc-sections *.wo

na2was: $(addsuffix .so, $(basename $(wildcard *.cpp)))
	$(CXX) -stdlib=libc++ -pthread !(na2was).so na2was.so -o $@

all: was.wasm na2was

clean:
	- rm *.so *.wo *.wasm na2was
