#include "was-memory.hpp"

#include <algorithm>

#include <boost/pool/simple_segregated_storage.hpp>

using namespace std;

extern "C" void growMemory(int32_t);
static constexpr int32_t chunk_size = sizeof(void *);
static_assert(sizeof(size_t) == chunk_size);
static boost::simple_segregated_storage<int32_t> memory;
static void addPages(int32_t pages) {
  int32_t initial_size = __builtin_wasm_memory_size(0) * (64 << 10);
  growMemory(pages);
  int32_t with_heap_size = __builtin_wasm_memory_size(0) * (64 << 10);
  int32_t heap_size = with_heap_size - initial_size;
  memory.add_ordered_block(reinterpret_cast<void *>(initial_size), heap_size,
                           chunk_size);
}

int32_t to_chunks(size_t count) {
  return (count / chunk_size) + ((count % chunk_size) > 0) + 1;
}

static void *get_chunks(size_t count) {
  auto newed =
      static_cast<uint8_t *>(memory.malloc_n(to_chunks(count), chunk_size));
  ::new (reinterpret_cast<size_t *>(newed)) size_t{count};
  return newed + chunk_size;
}

void *operator new(size_t count) {
  if (memory.empty())
    addPages(5);

  return get_chunks(count);
}
void *operator new[](size_t count) { return ::operator new(count); }
void *operator new(size_t count, align_val_t) { return ::operator new(count); }
void *operator new[](size_t count, align_val_t) {
  return ::operator new[](count);
}
void *operator new(size_t count, const nothrow_t &) noexcept {
  return ::operator new(count);
}
void *operator new[](size_t count, const nothrow_t &) noexcept {
  return ::operator new[](count);
}
void *operator new(size_t count, align_val_t align,
                   const nothrow_t &) noexcept {
  return ::operator new(count, align);
}
void *operator new[](size_t count, align_val_t align,
                     const nothrow_t &) noexcept {
  return ::operator new[](count, align);
}
void operator delete(void *ptr, size_t sz) noexcept {
  auto ostensible = static_cast<uint8_t *>(ptr);
  auto actual = ostensible - chunk_size;
  generate(actual, ostensible,
           [i = 0]() mutable { return i++ % 2 ? 'F' : 'D'; });
  memory.ordered_free_n(actual, to_chunks(sz), chunk_size);
}
void operator delete[](void *ptr, size_t sz) noexcept {
  ::operator delete(ptr, sz);
}
void operator delete(void *ptr, size_t sz, align_val_t) noexcept {
  ::operator delete(ptr, sz);
}
void operator delete[](void *ptr, size_t sz, align_val_t) noexcept {
  ::operator delete[](ptr, sz);
}
void operator delete(void *ptr) noexcept {
  auto ostensible = static_cast<uint8_t *>(ptr);
  auto actual = ostensible - chunk_size;
  ::operator delete(ptr, *reinterpret_cast<size_t *>(actual));
}
void operator delete[](void *ptr) noexcept { ::operator delete(ptr); }
void operator delete(void *ptr, align_val_t) noexcept {
  ::operator delete(ptr);
}
void operator delete[](void *ptr, align_val_t) noexcept {
  ::operator delete[](ptr);
}
