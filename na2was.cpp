#include "na2was.hpp"

#include "was-five-sql.hpp"

#include <iostream>

using namespace std;

int main([[maybe_unused]] int c, [[maybe_unused]] char **v) {
  Site site{{"./signups.db"}};

  site(Site::NewSignup{{"Person@1"}});
  site(Site::NewSignup{{"asdf"}});
  site(Site::NewSignup{{"fdsa"}});
  site(Site::NewSignup{{"a"}});
  site(Site::NewSignup{{"asdf"}});
  site(Site::NewSignup{{"b"}});
  site(Site::NewSignup{
      {"and (SELECT count(tbl_name) FROM sqlite_master WHERE type='table' "
       "and tbl_name NOT like 'sqlite_%' ) < number_of_table"}});
  site(Site::ChangeAccess{1, Site::SIGNUPS::Access::REGISTERED});
  site(Site::AddPerson{1});
  site(Site::ChangeAccess{2, Site::SIGNUPS::Access::REGISTERED});
  site(Site::AddPerson{2});

  Session session1{{"./signups.db"}, 1};
  session1(Session::NewPost{{"Title 1"}, {"Person 1 Content 1"}});
  session1(Session::NewPost{{"Title 2"}, {"Person 1 Content 2"}});
  session1(Session::NewPost{{"Title 3"}, {"Person 1 Content 3"}});

  Session session2{{"./signups.db"}, 2};
  session2(Session::NewPost{{"Title 1"}, {"Person 2 Content 1"}});
  session2(Session::NewPost{{"Title 2"}, {"Person 2 Content 2"}});
  session2(Session::NewPost{{"Title 3"}, {"Person 2 Content 3"}});

  cout << "Reading stream 1:\n";
  for (auto &&post : session1(Session::ReadStream{}))
    cout << post.CONTENT.value() << '\n';
  cout << '\n';

  cout << "Reading stream 2:\n";
  for (auto &&post : session2(Session::ReadStream{}))
    cout << post.CONTENT.value() << '\n';
  cout << '\n';

  cout << "Reading author 1:\n";
  for (auto &&post : session1(Session::ReadAuthor{1}))
    cout << post.CONTENT.value() << '\n';
  cout << '\n';

  cout << "Reading author 2:\n";
  for (auto &&post : session2(Session::ReadAuthor{2}))
    cout << post.CONTENT.value() << '\n';
  cout << '\n';

  cout << "Reading culture:\n";
  for (auto &&post : session1(Session::ReadCulture{1}))
    cout << post.CONTENT.value() << '\n';
  cout << '\n';

  cout << "Reading culture:\n";
  for (auto &&post : session2(Session::ReadCulture{1}))
    cout << post.CONTENT.value() << '\n';
  cout << '\n';

  string end;
  cin >> end;

  return 0;
}
