#pragma once

template <typename... Args> inline constexpr void atof(Args &&...);
template <typename... Args> inline constexpr void atoi(Args &&...);
template <typename... Args> inline constexpr void atol(Args &&...);
template <typename... Args> inline constexpr void atoll(Args &&...);
template <typename... Args> inline constexpr void strtod(Args &&...);
template <typename... Args> inline constexpr void strtof(Args &&...);
template <typename... Args> inline constexpr void strtol(Args &&...);
template <typename... Args> inline constexpr void strtold(Args &&...);
template <typename... Args> inline constexpr void strtoll(Args &&...);
template <typename... Args> inline constexpr void strtoul(Args &&...);
template <typename... Args> inline constexpr void strtoull(Args &&...);

template <typename... Args> inline constexpr void _Exit(Args &&...);
extern "C" void abort(const char * = "Unrecoverable error.");
template <typename... Args> inline constexpr void atexit(Args &&...);
template <typename... Args> inline constexpr void bsearch(Args &&...);
template <typename... Args> inline constexpr void calloc(Args &&...);
template <typename... Args> inline constexpr void exit(Args &&...);
template <typename... Args> inline constexpr void free(Args &&...);
template <typename... Args> inline constexpr void getenv(Args &&...);
template <typename... Args> inline constexpr void malloc(Args &&...);
template <typename... Args> inline constexpr void mblen(Args &&...);
template <typename... Args> inline constexpr void mbtowc(Args &&...);
template <typename... Args> inline constexpr void mbstowcs(Args &&...);
template <typename... Args> inline constexpr void qsort(Args &&...);
template <typename... Args> inline constexpr void rand(Args &&...);
template <typename... Args> inline constexpr void realloc(Args &&...);
template <typename... Args> inline constexpr void srand(Args &&...);
template <typename... Args> inline constexpr void system(Args &&...);
template <typename... Args> inline constexpr void wctomb(Args &&...);
template <typename... Args> inline constexpr void wcstombs(Args &&...);

class tm;
