#pragma once

#include <cstddef>

struct clock_t;
using time_t = __int128_t;
struct tm;

template <typename... Args> inline constexpr void clock(Args &&...);
template <typename... Args> inline constexpr void difftime(Args &&...);
template <typename... Args> inline constexpr void mktime(Args &&...);
template <typename... Args> inline constexpr void time(Args &&...);
template <typename... Args> inline constexpr void asctime(Args &&...);
template <typename... Args> inline constexpr void ctime(Args &&...);
template <typename... Args> inline constexpr void gmtime(Args &&...);
template <typename... Args> inline constexpr void localtime(Args &&...);
template <typename... Args> inline constexpr void strftime(Args &&...);
