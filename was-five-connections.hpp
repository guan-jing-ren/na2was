#pragma once

#include "was-five-sql.hpp"

#include <condition_variable>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>

template <typename KeyType, typename MappedType> struct Queue {
  bool empty() const {
    std::lock_guard lock{m_queue_mx};
    return m_queue.empty();
  }

  void push(KeyType key, MappedType value) {
    std::unordered_map<KeyType, MappedType> insertion;
    insertion.emplace(std::move(key), std::move(value));
    [[maybe_unused]] std::lock_guard lock{m_queue_mx};
    m_queue.merge(std::move(insertion));
  }

  MappedType pop(const KeyType &key) {
    typename decltype(m_queue)::node_type value_node;
    {
      [[maybe_unused]] std::lock_guard lock{m_queue_mx};
      value_node = m_queue.extract(key);
    }
    return move(value_node.mapped());
  }

private:
  mutable std::mutex m_queue_mx;
  std::unordered_map<KeyType, MappedType> m_queue;
};

struct Connections {
  Connections();

private:
  mutable std::mutex m_request_mx;
  mutable std::condition_variable m_request_cv;
  Queue<std::string, Session> m_sessions;
  Queue<std::string, std::function<void(const Session &)>> m_requests;
  std::vector<std::thread> m_pool;
};
