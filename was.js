'use strict';
let wasm;
let __NA2WASMemory;
function __NA2WASLittleEndian() {
    return wasm.instance.exports.is_little_endian();
}
function __NA2WASArgumentSize() {
    return wasm.instance.exports.argument_size();
}
function __NA2WASGetPtr(ptr) {
    return new Uint8Array(__NA2WASMemory.buffer, ptr);
}
function __NA2WASGetData(ptr, length) {
    return new DataView(__NA2WASMemory.buffer, ptr, length);
}
function __NA2WASDataSlice(data, offset, size) {
    return __NA2WASGetData(data.byteOffset + offset, size);
}
let decoder = new TextDecoder("utf-8");
function __NA2WASPtrToString(ptr) {
    let memory = __NA2WASGetPtr(ptr);
    return decoder.decode(memory.subarray(0, memory.indexOf(0)));
}
let __NA2WASAllocatedObjects = { "-1": { "-1": null }, 0: { 0: window } };
let __NA2WASMaxObjectHi = 0;
let __NA2WASMaxObjectLo = 1;
let __NA2WASGarbageCollector = window.setInterval(function (start) {
    __NA2WASMaxObjectHi = Math.max(...Object.keys(__NA2WASAllocatedObjects));
    __NA2WASMaxObjectLo = Math.max(...Object.keys(__NA2WASAllocatedObjects[__NA2WASMaxObjectHi])) + 1;
}, 5 * 60 * 1000, Date.now());
function __NA2WASAllocateObject(o) {
    __NA2WASAllocatedObjects[__NA2WASMaxObjectHi] = __NA2WASAllocatedObjects[__NA2WASMaxObjectHi] || {};
    __NA2WASAllocatedObjects[__NA2WASMaxObjectHi][__NA2WASMaxObjectLo] = o;
    let alloc = { hi: __NA2WASMaxObjectHi, lo: __NA2WASMaxObjectLo++ };
    if (__NA2WASMaxObjectLo === (Math.pow(2, 32) - 1)) {
        __NA2WASAllocatedObjects[++__NA2WASMaxObjectHi] = {};
        __NA2WASMaxObjectLo = 0;
    }

    return alloc;
}
function __NA2WASConvertArgument(argv, argt, offset, size, ...extra) {
    switch (argt) {
        default:
            console.log(argt);
            break;
        case 0:
            return argv['getInt' + (size || 4) * 8](offset, __NA2WASLittleEndian());
        case 1:
            return argv['getFloat' + (size || 8) * 8](offset, __NA2WASLittleEndian());
        case 2:
            return __NA2WASPtrToString(argv.getInt32(offset, __NA2WASLittleEndian()));
        case 3:
            return __NA2WASAllocatedObjects[argv.getInt32(offset, __NA2WASLittleEndian())][argv.getInt32(offset + __NA2WASArgumentSize() / 2, __NA2WASLittleEndian())];
        case 4: {
            let f = argv.getInt32(offset, __NA2WASLittleEndian());
            return function (...invokeArgs) {
                let alloc = __NA2WASAllocateObject(invokeArgs);
                wasm.instance.exports.invoke_static(f, alloc.hi, alloc.lo);
            };
        }
        case 5: {
            let f = argv.getInt32(offset, __NA2WASLittleEndian());
            return function (...invokeArgs) {
                let alloc = __NA2WASAllocateObject(invokeArgs);
                let rv = {};
                let rvalloc = __NA2WASAllocateObject(rv);
                wasm.instance.exports.invoke_dynamic(f, alloc.hi, alloc.lo, rvalloc.hi, rvalloc.lo);
                return rv.value;
            };
        }
        case 6: {
            let [mangled] = extra;
            return __NA2WASCreateNativeObject(mangled, argv.byteOffset + offset);
        }
    }
}
function __NA2WASHandleReturn(retv, rett, result, size) {
    retv = __NA2WASGetData(retv, __NA2WASArgumentSize());
    rett = __NA2WASGetData(rett, 1);

    let result_type = typeof result;
    if (result_type === 'object' || result_type === 'string' || result_type === 'function') {
        rett.setInt8(0, 3);
        let alloc = __NA2WASAllocateObject(result);
        retv.setInt32(0, alloc.hi, __NA2WASLittleEndian());
        retv.setInt32(__NA2WASArgumentSize() / 2, alloc.lo, __NA2WASLittleEndian());
    }
    else if (result_type === 'number' || result_type === 'boolean') {
        if (Number.isInteger(result) || result_type === 'boolean') {
            rett.setInt8(0, 0);
            retv['setInt' + (size || 4) * 8](0, result, __NA2WASLittleEndian());
        }
        else {
            rett.setInt8(0, 1);
            retv['setFloat' + (size || 8) * 8](0, result, __NA2WASLittleEndian());
        }
    }
    else rett.setInt8(0, -1);
}
let __NA2WASNativeDefinitions = new Map();
function __NA2WASDefineNativeObject(mangled, size, members) {
    if (__NA2WASNativeDefinitions.has(mangled))
        return;

    let properties = {
        ownKeys: function (target) {
            let keys = [];
            if (members['__NA2WAS::operator[]'])
                for (let i = 0, size = members['__NA2WAS::size'](target.__NA2WASNativeArray.byteOffset); i < size; ++i)
                    keys.push(i.toString());
            else keys = Object.getOwnPropertyNames(members).filter(v => !v.startsWith('__NA2WAS::'));

            return keys;
        },
        get: function (target, property, receiver) {
            let ptr = target.__NA2WASNativeArray.byteOffset;
            if (property === '__NA2WAS::size')
                return members['__NA2WAS::size'](ptr);
            if (property === '__NA2WAS::value')
                return members['__NA2WAS::value'](ptr);
            else if (property === Symbol.iterator)
                return function () {
                    return {
                        i: 0,
                        size: receiver['__NA2WAS::size'],
                        next:
                            members['__NA2WAS::iterator'] ? ((iterator) => {
                                return function () {
                                    return iterator() || { done: true };
                                };
                            })(members['__NA2WAS::iterator'](ptr))
                                : (members['__NA2WAS::operator[]'] ? function () {
                                    if (this.i < this.size)
                                        return { done: false, value: receiver[this.i++] };
                                    return { done: true };
                                }
                                    :
                                    function () {
                                        return { done: true };
                                    })
                    };
                };
            else
                try {
                    let i = Number.parseInt(property);
                    if (!Number.isNaN(i))
                        return members['__NA2WAS::operator[]'](ptr, i);
                }
                catch (e) { }

            return members[property](ptr);
        }
    }

    __NA2WASNativeDefinitions.set(mangled, function (ptr) {
        this.__NA2WASNativeArray = __NA2WASGetData(ptr, size);
        this.__NA2WASProxy = new Proxy(this, properties);
    });
}
function __NA2WASCreateNativeObject(mangled, ptr) {
    return (new (__NA2WASNativeDefinitions.get(mangled))(ptr)).__NA2WASProxy;
}
const wasImports = {
    env: {
        memory: __NA2WASMemory = new WebAssembly.Memory({ initial: 2 }),
        abort: function (msg) { throw "NA2WAS aborted: " + __NA2WASPtrToString(msg); },
        debug: function (msg) { console.log(__NA2WASPtrToString(msg)); },
        growMemory: function (pages) { __NA2WASMemory.grow(pages); },
        getProperty: function (hi, lo, name, isindex, retv, rett) {
            if (name === 0 && !isindex)
                return window[lo];

            if (!isindex)
                name = __NA2WASPtrToString(name);
            let result = __NA2WASAllocatedObjects[hi][lo][name];
            __NA2WASHandleReturn(retv, rett, result);
        },
        setProperty: function (hi, lo, name, isindex, argv, argt) {
            if (!isindex)
                name = __NA2WASPtrToString(name);
            __NA2WASAllocatedObjects[hi][lo][name] = __NA2WASConvertArgument(__NA2WASGetData(argv), argt, 0);
        },
        applyWithArgs: function (construct, prop_hi, prop_lo, obj_hi, obj_lo, argv, argt, argc, retv, rett) {
            let args = [];
            if (argc > 0) {
                argv = __NA2WASGetData(argv, argc * __NA2WASArgumentSize());
                argt = __NA2WASGetData(argt, argc);
                let offset = 0;
                for (let i = 0; i < argc; ++i, offset += __NA2WASArgumentSize())
                    args[i] = __NA2WASConvertArgument(argv, argt.getInt8(i), offset);
            }

            let result;
            if (!construct)
                result = __NA2WASAllocatedObjects[prop_hi][prop_lo].apply(__NA2WASAllocatedObjects[obj_hi][obj_lo], args);
            else
                result = new __NA2WASAllocatedObjects[prop_hi][prop_lo](...args);

            __NA2WASHandleReturn(retv, rett, result);
        },
        releaseReference: function (hi, lo) {
            if (hi === 0 && lo === 0)
                return;

            delete __NA2WASAllocatedObjects[hi][lo];
            if (Object.keys(__NA2WASAllocatedObjects[hi]).length == 0)
                delete __NA2WASAllocatedObjects[hi];
        }
        ,
        fabs: Math.abs,
        fabsf: Math.abs,
        fabsl: Math.abs,
        labs: Math.abs,
        llabs: Math.abs,
        acos: Math.acos,
        acosf: Math.acos,
        acosl: Math.acos,
        acosh: Math.acosh,
        acoshf: Math.acosh,
        acoshl: Math.acosh,
        asin: Math.asin,
        asinf: Math.asin,
        asinl: Math.asin,
        asinh: Math.asinh,
        asinhf: Math.asinh,
        asinhl: Math.asinh,
        atan: Math.atan,
        atanf: Math.atan,
        atanl: Math.atan,
        atan2: Math.atan2,
        atan2f: Math.atan2,
        atan2l: Math.atan2,
        atanh: Math.atanh,
        atanhf: Math.atanh,
        atanhl: Math.atanh,
        cbrt: Math.cbrt,
        cbrtf: Math.cbrt,
        cbrtl: Math.cbrt,
        ceil: Math.ceil,
        ceilf: Math.ceil,
        ceill: Math.ceil,
        cos: Math.cos,
        cosf: Math.cos,
        cosl: Math.cos,
        cosh: Math.cosh,
        coshf: Math.cosh,
        coshl: Math.cosh,
        exp: Math.exp,
        expf: Math.exp,
        expl: Math.exp,
        expm1: Math.expm1,
        expm1f: Math.expm1,
        expm1l: Math.expm1,
        floor: Math.floor,
        floorf: Math.floor,
        floorl: Math.floor,
        fmax: Math.max,
        fmaxf: Math.max,
        fmaxl: Math.max,
        fmin: Math.min,
        fminf: Math.min,
        fminl: Math.min,
        // fround: Math.fround,
        hypot: Math.hypot,
        hypotf: Math.hypot,
        hypotl: Math.hypot,
        // imul: Math.imul,
        isfinite: Number.isFinite,
        isnan: Number.isNaN,
        log: Math.log,
        logf: Math.log,
        logl: Math.log,
        log10: Math.log10,
        log10f: Math.log10,
        log10l: Math.log10,
        log1p: Math.log1p,
        log1pf: Math.log1p,
        log1pl: Math.log1p,
        log2: Math.log2,
        log2f: Math.log2,
        log2l: Math.log2,
        pow: Math.pow,
        powf: Math.pow,
        powl: Math.pow,
        // random: Math.random,
        round: Math.round,
        roundf: Math.round,
        roundl: Math.round,
        signbit: n => Math.sign(n) < 0,
        sin: Math.sin,
        sinf: Math.sin,
        sinl: Math.sin,
        sinh: Math.sinh,
        sinhf: Math.sinh,
        sinhl: Math.sinh,
        sqrt: Math.sqrt,
        sqrtf: Math.sqrt,
        sqrtl: Math.sqrt,
        tan: Math.tan,
        tanf: Math.tan,
        tanl: Math.tan,
        tanh: Math.tanh,
        tanhf: Math.tanh,
        tanhl: Math.tanh,
        trunc: Math.trunc,
        truncf: Math.trunc,
        truncl: Math.trunc
    }
};
WebAssembly.instantiateStreaming(fetch('was.wasm'), wasImports)
    .then(obj => wasm = obj).then(() => wasm.instance.exports.init())
    .then(() => console.log("Still allocated: ", __NA2WASAllocatedObjects));
