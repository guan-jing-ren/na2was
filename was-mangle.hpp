#pragma once

#include "was.hpp"

#include "../gut/bitfield.hpp"
#include "../gut/fstring.hpp"
#include "../gut/integer.hpp"
#include "../gut/reflect.hpp"
#include "../gut/variant.hpp"

#include <deque>
#include <list>
#include <map>
#include <set>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

GUT_TYPEDEF(mangle_value_typed, value_type);
GUT_TYPEDEF(mangle_key_typed, key_type);
GUT_TYPEDEF(mangle_mapped_typed, mapped_type);
GUT_TYPEDEF(mangle_size_typed, size_type);
GUT_MEMFN(mangle_sized, size());

template <typename T>
inline constexpr gut::constant is_sequence_container_v =
    gut::constant_v<is_mangle_value_typed_v<T> && !is_mangle_key_typed_v<T> &&
                    !is_mangle_mapped_typed_v<T> && is_mangle_size_typed_v<T> &&
                    is_mangle_sized_v<T>>;

template <typename T>
inline constexpr gut::constant is_associative_container_v =
    gut::constant_v<is_mangle_value_typed_v<T> && is_mangle_key_typed_v<T> &&
                    is_mangle_mapped_typed_v<T> && is_mangle_size_typed_v<T> &&
                    is_mangle_sized_v<T>>;

template <typename... Args>
inline constexpr gut::constant is_vector_v = gut::constant_v<false>;
template <typename... Args>
inline constexpr gut::constant is_string_v = gut::constant_v<false>;
template <typename... Args>
inline constexpr gut::constant is_deque_v = gut::constant_v<false>;
template <typename... Args>
inline constexpr gut::constant is_list_v = gut::constant_v<false>;
template <typename... Args>
inline constexpr gut::constant is_set_v = gut::constant_v<false>;
template <typename... Args>
inline constexpr gut::constant is_multiset_v = gut::constant_v<false>;
template <typename... Args>
inline constexpr gut::constant is_unordered_set_v = gut::constant_v<false>;
template <typename... Args>
inline constexpr gut::constant is_unordered_multiset_v = gut::constant_v<false>;
template <typename... Args>
inline constexpr gut::constant is_map_v = gut::constant_v<false>;
template <typename... Args>
inline constexpr gut::constant is_multimap_v = gut::constant_v<false>;
template <typename... Args>
inline constexpr gut::constant is_unordered_map_v = gut::constant_v<false>;
template <typename... Args>
inline constexpr gut::constant is_unordered_multimap_v = gut::constant_v<false>;

template <typename... Args>
inline constexpr gut::constant is_vector_v<std::vector<Args...>> =
    gut::constant_v<true>;
template <typename... Args>
inline constexpr gut::constant is_string_v<std::basic_string<Args...>> =
    gut::constant_v<true>;
template <typename... Args>
inline constexpr gut::constant is_deque_v<std::deque<Args...>> =
    gut::constant_v<true>;
template <typename... Args>
inline constexpr gut::constant is_list_v<std::list<Args...>> =
    gut::constant_v<true>;
template <typename... Args>
inline constexpr gut::constant is_set_v<std::set<Args...>> =
    gut::constant_v<true>;
template <typename... Args>
inline constexpr gut::constant is_multiset_v<std::set<Args...>> =
    gut::constant_v<true>;
template <typename... Args>
inline constexpr gut::constant is_unordered_set_v<std::unordered_set<Args...>> =
    gut::constant_v<true>;
template <typename... Args>
inline constexpr gut::constant
    is_unordered_multiset_v<std::unordered_set<Args...>> =
        gut::constant_v<true>;
template <typename... Args>
inline constexpr gut::constant is_map_v<std::map<Args...>> =
    gut::constant_v<true>;
template <typename... Args>
inline constexpr gut::constant is_multimap_v<std::map<Args...>> =
    gut::constant_v<true>;
template <typename... Args>
inline constexpr gut::constant is_unordered_map_v<std::unordered_map<Args...>> =
    gut::constant_v<true>;
template <typename... Args>
inline constexpr gut::constant
    is_unordered_multimap_v<std::unordered_map<Args...>> =
        gut::constant_v<true>;

struct mangle_environment_base {
protected:
  template <typename T>
  struct fstring_filter : decltype(gut::is_fstring_v<T>) {};

  template <auto BitSize>
  inline constexpr auto bitlog(gut::constant<BitSize>) const {
    return mangle_constant(
        gut::constant_v<
            decltype(gut::integer{gut::constant_v<BitSize - 1>})::count() - 1>);
  }

  template <auto Value>
  inline constexpr auto mangle_constant(gut::constant<Value> i) const {
    constexpr auto log = [](gut::count_t i) {
      if (i == 0)
        return ++i;
      gut::count_t log = 0;
      for (; i; i /= 10, ++log)
        ;
      return log;
    }(i);
    gut::fstring<char, log> num;
    for (gut::count_t c = num.count(), n = i; c > 0; n /= 10, --c)
      num[c - 1] = '0' + n % 10;
    return num;
  }
};

template <typename... Reflected>
struct mangle_environment : private mangle_environment_base,
                            private gut::ntuple<Reflected...> {
  static_assert((true && ... &&
                 (gut::is_reflect_enum_v<Reflected> ||
                  gut::is_reflect_struct_v<Reflected> ||
                  gut::is_reflect_namespace_v<Reflected>)));

  constexpr mangle_environment(Reflected... reflected)
      : gut::ntuple<Reflected...>(reflected...) {}

  template <typename Action, typename ReflectedType>
  inline constexpr auto traverse(Action &&action,
                                 ReflectedType reflected) const {
    if constexpr (gut::is_reflect_struct_v<ReflectedType> ||
                  gut::is_reflect_enum_v<ReflectedType>)
      return action(reflected);
    else if constexpr (gut::is_reflect_namespace_v<ReflectedType>)
      return this->traverse(action, reflected.members());
    else if constexpr (!gut::is_reflect_overloads_v<ReflectedType>)
      return reflected.map([this, &action](auto reflect) {
        return this->traverse(action, reflect);
      });
  }

  template <typename Action>
  inline constexpr auto traverse(Action &&action) const {
    return traverse(action, *this);
  }

  template <typename Type>
  inline constexpr auto mangle(gut::type<Type> type) const {
    if constexpr (type.is_fundamental()) {
      if constexpr (type == gut::type_v<void>)
        return gut::fstring{"v"};
      else if constexpr (type == gut::type_v<bool>)
        return gut::fstring{"b"};
      else if constexpr (type == gut::type_v<char> ||
                         type == gut::type_v<char16_t> ||
                         type == gut::type_v<char32_t>)
        return "'" + bitlog(gut::constant_v<sizeof(Type) * CHAR_BIT>);
      else if constexpr (type == gut::type_v<wchar_t>)
        return gut::fstring{"'w"};
      else if constexpr (type.is_integral())
        return (type.is_signed() ? "i" : "u") +
               bitlog(gut::constant_v<sizeof(Type) * CHAR_BIT>);
      else if constexpr (type.is_floating_point())
        return "f" + bitlog(gut::constant_v<sizeof(Type) * CHAR_BIT>);
      else
        return gut::fstring{"z"};
    } else if constexpr (gut::is_integer_v<Type>) {
      return mangle(Type::value_type_v());
    } else if constexpr (gut::is_bitfield_v<Type>) {
      auto mangled = Type::offsets_v().reduce([this](auto... offsets) {
        return (... + ("," + this->mangle_constant(
                                 gut::constant_v<offsets.value()>)));
      });
      mangled[0] = '!';
      return mangled;
    } else if constexpr (gut::is_buffer_v<Type>) {
      return "[" + mangle(Type::value_type_v()) + "," +
             mangle_constant(gut::constant_v<Type::count().value()>);
    } else if constexpr (gut::is_ntuple_v<Type>) {
      if constexpr (Type::count() > 0) {
        auto mangled =
            Type::member_types_v().reduce([this](auto... member_types) {
              return (gut::fstring{""} + ... +
                      ("," + this->mangle(member_types.remove_cvref())));
            }) +
            ")";
        mangled[0] = '(';
        return mangled;
      } else
        return gut::fstring{"()"};
    } else if constexpr (gut::is_optional_v<Type>)
      return "?" + mangle(Type::value_type_v().remove_cvref());
    else if constexpr (gut::is_variant_v<Type>) {
      auto mangled =
          Type::member_types_v().reduce([this](auto... member_types) {
            return (... + ("," + this->mangle(member_types.remove_cvref())));
          }) +
          ">";
      mangled[0] = '<';
      return mangled;
    } else if constexpr (is_sequence_container_v<Type>)
      return "*\"" +
             []() {
               if constexpr (is_vector_v<Type>)
                 return gut::fstring{"vector"};
               else if constexpr (is_string_v<Type>)
                 return gut::fstring{"string"};
               else if constexpr (is_deque_v<Type>)
                 return gut::fstring{"deque"};
               else if constexpr (is_list_v<Type>)
                 return gut::fstring{"list"};
               else if constexpr (is_set_v<Type>)
                 return gut::fstring{"set"};
               else if constexpr (is_multiset_v<Type>)
                 return gut::fstring{"multiset"};
               else if constexpr (is_unordered_set_v<Type>)
                 return gut::fstring{"unordered_set"};
               else if constexpr (is_unordered_multiset_v<Type>)
                 return gut::fstring{"unordered_multiset"};
               else
                 return gut::fstring{""};
             }() +
             "\"" + mangle(gut::type_v<typename Type::size_type>) + "," +
             mangle(gut::type_v<typename Type::value_type>);
    else if constexpr (is_associative_container_v<Type>)
      return ":\"" +
             []() {
               if constexpr (is_map_v<Type>)
                 return gut::fstring{"map"};
               else if constexpr (is_multimap_v<Type>)
                 return gut::fstring{"multimap"};
               else if constexpr (is_unordered_map_v<Type>)
                 return gut::fstring{"unordered_map"};
               else if constexpr (is_unordered_multimap_v<Type>)
                 return gut::fstring{"unordered_multimap"};
               else
                 return gut::fstring{""};
             }() +
             "\"" + mangle(gut::type_v<typename Type::size_type>) + "," +
             mangle(gut::type_v<typename Type::key_type>) + "," +
             mangle(gut::type_v<typename Type::mapped_type>);
    else
      return traverse([this, type](auto reflect) {
               using ReflectedType = typename decltype(
                   gut::type_v<decltype(reflect)>.remove_cvref())::raw_type;
               if constexpr (gut::is_reflect_struct_v<ReflectedType>) {
                 if constexpr (reflect.type_v() == type)
                   return "\"" + reflect.name() + "\"" +
                          reflect.members()
                              .map([](auto member) {
                                return gut::type_v<decltype(std::declval<Type>().*member.value)>.remove_cvref();
                              })
                              .reduce([this](auto... member_types) {
                                return this->mangle(
                                    gut::type_v<gut::ntuple<typename decltype(
                                        member_types)::raw_type...>>);
                              });
               } else if constexpr (gut::is_reflect_enum_v<ReflectedType>) {
                 if constexpr (reflect.type_v() == type) {
                   constexpr auto underlying =
                       reflect.type_v().underlying_type();
                   return reflect.members().reduce([this, underlying](
                                                       auto... member_values) {
                     return "{" + mangle(underlying) +
                            (... + ("," + mangle_constant(
                                              gut::constant_v<underlying.cast(
                                                  member_values)>))) +
                            "}";
                   });
                 }
               }
             })
          .template filter<fstring_filter>()
          .front();
  }
};
