#pragma once

#include <array>
#include <vector>

std::vector<std::array<double, 2>> ease_in_linear();
std::vector<std::array<double, 2>> ease_in_circular();
std::vector<std::array<double, 2>> ease_in_quadratic();
std::vector<std::array<double, 2>> ease_in_cubic();
std::vector<std::array<double, 2>> ease_in_quartic();
std::vector<std::array<double, 2>> ease_in_quintic();
std::vector<std::array<double, 2>> ease_in_sinusoidal();
std::vector<std::array<double, 2>> ease_in_exponential();
std::vector<std::array<double, 2>> ease_in_back();
std::vector<std::array<double, 2>> ease_in_elastic();
std::vector<std::array<double, 2>> ease_in_bounce();
std::vector<std::array<double, 2>> ease_in_shake();

std::vector<std::array<double, 2>> ease_out_linear();
std::vector<std::array<double, 2>> ease_out_circular();
std::vector<std::array<double, 2>> ease_out_quadratic();
std::vector<std::array<double, 2>> ease_out_cubic();
std::vector<std::array<double, 2>> ease_out_quartic();
std::vector<std::array<double, 2>> ease_out_quintic();
std::vector<std::array<double, 2>> ease_out_sinusoidal();
std::vector<std::array<double, 2>> ease_out_exponential();
std::vector<std::array<double, 2>> ease_out_back();
std::vector<std::array<double, 2>> ease_out_elastic();
std::vector<std::array<double, 2>> ease_out_bounce();
std::vector<std::array<double, 2>> ease_out_shake();

std::vector<std::array<double, 2>> ease_in_out_linear();
std::vector<std::array<double, 2>> ease_in_out_circular();
std::vector<std::array<double, 2>> ease_in_out_quadratic();
std::vector<std::array<double, 2>> ease_in_out_cubic();
std::vector<std::array<double, 2>> ease_in_out_quartic();
std::vector<std::array<double, 2>> ease_in_out_quintic();
std::vector<std::array<double, 2>> ease_in_out_sinusoidal();
std::vector<std::array<double, 2>> ease_in_out_exponential();
std::vector<std::array<double, 2>> ease_in_out_back();
std::vector<std::array<double, 2>> ease_in_out_elastic();
std::vector<std::array<double, 2>> ease_in_out_bounce();
std::vector<std::array<double, 2>> ease_in_out_shake();

std::vector<std::array<double, 2>> ease_out_in_linear();
std::vector<std::array<double, 2>> ease_out_in_circular();
std::vector<std::array<double, 2>> ease_out_in_quadratic();
std::vector<std::array<double, 2>> ease_out_in_cubic();
std::vector<std::array<double, 2>> ease_out_in_quartic();
std::vector<std::array<double, 2>> ease_out_in_quintic();
std::vector<std::array<double, 2>> ease_out_in_sinusoidal();
std::vector<std::array<double, 2>> ease_out_in_exponential();
std::vector<std::array<double, 2>> ease_out_in_back();
std::vector<std::array<double, 2>> ease_out_in_elastic();
std::vector<std::array<double, 2>> ease_out_in_bounce();
std::vector<std::array<double, 2>> ease_out_in_shake();

std::vector<std::array<double, 2>> ease_flip_linear();
std::vector<std::array<double, 2>> ease_flip_circular();
std::vector<std::array<double, 2>> ease_flip_quadratic();
std::vector<std::array<double, 2>> ease_flip_cubic();
std::vector<std::array<double, 2>> ease_flip_quartic();
std::vector<std::array<double, 2>> ease_flip_quintic();
std::vector<std::array<double, 2>> ease_flip_sinusoidal();
std::vector<std::array<double, 2>> ease_flip_exponential();
std::vector<std::array<double, 2>> ease_flip_back();
std::vector<std::array<double, 2>> ease_flip_elastic();
std::vector<std::array<double, 2>> ease_flip_bounce();
std::vector<std::array<double, 2>> ease_flip_shake();

std::vector<std::array<double, 2>> regular_ngon(int sides, double smoothness);

std::vector<std::array<double, 2>>
interpolate(std::vector<std::array<double, 2>> points, bool closed = false,
            double granularity = .1);
