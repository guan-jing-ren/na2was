#pragma once

#include "was-five-operations.hpp"
#include "was-five-status.hpp"

#include <unordered_map>
#include <vector>

struct Person {
  struct Id {
  } id;
  struct Nickname {
  } nickname;
  struct Email {
  } email;
  struct Credentials {
  } credentials;
  enum Membership { FREE, SUBSIDIZED, PAID } membership;
  enum Relationship { TRUSTED, FAMILY, FRIEND, COLLEAGUE, CONTACT };
  std::unordered_map<Person, Relationship> friends;
  std::vector<struct Post> posts;
  std::vector<struct Culture> cultures;
  std::vector<struct Person> blocked_people;
  std::vector<struct Culture> blocked_cultures;
  std::vector<struct Post> muted_posts;
  std::vector<struct Person> muted_people;
  std::vector<struct Cultures> muted_cultures;
  std::vector<struct Bid> bids;
};

bool operator==(const Person &l, const Person &r);

// email and nickname exists -> PersonAlreadyExists
SessionCode<SuccessfulOperation::PersonModified,
            ErrorAlreadyExists::PersonAlreadyExists>
handle(gut::constant<Operation::ChangeNickname>, const struct Session &session,
       const Person::Nickname &nickname);

// email and nickname exists -> PersonAlreadyExists
SessionCode<SuccessfulOperation::PersonModified,
            ErrorAlreadyExists::PersonAlreadyExists>
handle(gut::constant<Operation::ChangeEmail>, const struct Session &session,
       const Person::Email &email);

SessionCode<SuccessfulOperation::PersonModified>
handle(gut::constant<Operation::ChangeCredentials>,
       const struct Session &session, const Person::Credentials &credentials);

SessionCode<
    SuccessfulOperation::PersonModified /* , MembershipPaymentInvalid */>
handle(gut::constant<Operation::UpgradeMembership>,
       const struct Session &session, Person::Membership membership);

SessionCode<
    SuccessfulOperation::PersonModified /* , MembershipPaymentInvalid */>
handle(gut::constant<Operation::DowngradeMembership>,
       const struct Session &session, Person::Membership membership);

// person blocked session.person -> SessionNotAuthorized
// session.person blocked person -> PersonNotAuthorized
SessionCode<SuccessfulOperation::PersonRelated, ErrorNotExists::PersonNotExists,
            ErrorNotAuthorized::PersonNotAuthorized,
            ErrorAlreadyExists::RelationAlreadyExists>
handle(gut::constant<Operation::AddRelation>, const struct Session &session,
       const Person &relation, Person::Relationship relationship);

// person blocked session.person -> SessionNotAuthorized
// session.person blocked person -> PersonNotAuthorized
SessionCode<SuccessfulOperation::PersonRelated, ErrorNotExists::PersonNotExists,
            ErrorNotAuthorized::PersonNotAuthorized,
            ErrorNotExists::RelationNotExists>
handle(gut::constant<Operation::ChangeRelation>, const struct Session &session,
       const Person &relation, Person::Relationship relationship);

// Unilateral
SessionCode<SuccessfulOperation::PersonUnrelated,
            ErrorNotExists::PersonNotExists, ErrorNotExists::RelationNotExists>
handle(gut::constant<Operation::RemoveRelation>, const struct Session &session,
       const Person &relation);

// culture banned session.person -> SessionNotAuthorized
// session.person blocked culture -> CultureNotAuthorized
// session.person in culture.organizers, culture.moderators,
// culture.participants -> ParticipationAlreadyExists
SessionCode<SuccessfulOperation::PersonParticipated,
            ErrorNotExists::CultureNotExists,
            ErrorNotAuthorized::CultureNotAuthorized,
            ErrorAlreadyExists::ParticipationAlreadyExists>
handle(gut::constant<Operation::JoinCulture>, const struct Session &session,
       const Culture &culture);

// Unilateral
// session.person in culture.organizers, culture.moderators,
// culture.participants -> SessionNotAuthorized
SessionCode<SuccessfulOperation::PersonUnparticipated,
            ErrorNotExists::CultureNotExists,
            ErrorNotExists::ParticipationNotExists>
handle(gut::constant<Operation::LeaveCulture>, const struct Session &session,
       const Culture &culture);

// Unilateral
SessionCode<SuccessfulOperation::PersonBlocked, ErrorNotExists::PersonNotExists,
            ErrorAlreadyExists::BlockAlreadyExists>
handle(gut::constant<Operation::Block>, const struct Session &session,
       const Person &person);

// Unilateral
SessionCode<SuccessfulOperation::PersonBlocked,
            ErrorNotExists::CultureNotExists,
            ErrorAlreadyExists::BlockAlreadyExists>
handle(gut::constant<Operation::Block>, const struct Session &session,
       const Culture &culture);

// Unilateral
SessionCode<SuccessfulOperation::PersonUnBlocked,
            ErrorNotExists::PersonNotExists, ErrorNotExists::BlockNotExists>
handle(gut::constant<Operation::Unblock>, const struct Session &session,
       const Person &person);

// Unilateral
SessionCode<SuccessfulOperation::PersonUnBlocked,
            ErrorNotExists::CultureNotExists, ErrorNotExists::BlockNotExists>
handle(gut::constant<Operation::Unblock>, const struct Session &session,
       const Culture &culture);

// Unilateral
SessionCode<SuccessfulOperation::PostMuted, ErrorNotExists::PostNotExists,
            ErrorAlreadyExists::MuteAlreadyExists>
handle(gut::constant<Operation::Mute>, const struct Session &session,
       const Post &post);

// Unilateral
SessionCode<SuccessfulOperation::PostMuted, ErrorNotExists::PostNotExists,
            ErrorNotExists::MuteNotExists>
handle(gut::constant<Operation::Unmute>, const struct Session &session,
       const Post &post);

// Unilateral
SessionCode<SuccessfulOperation::PostMuted, ErrorNotExists::PostNotExists,
            ErrorAlreadyExists::MuteAlreadyExists>
handle(gut::constant<Operation::Mute>, const struct Session &session,
       const Person &person);

// Unilateral
SessionCode<SuccessfulOperation::PostMuted, ErrorNotExists::PostNotExists,
            ErrorNotExists::MuteNotExists>
handle(gut::constant<Operation::Unmute>, const struct Session &session,
       const Person &person);

// Unilateral
SessionCode<SuccessfulOperation::PostMuted, ErrorNotExists::PostNotExists,
            ErrorAlreadyExists::MuteAlreadyExists>
handle(gut::constant<Operation::Mute>, const struct Session &session,
       const Culture &culture);

// Unilateral
SessionCode<SuccessfulOperation::PostMuted, ErrorNotExists::PostNotExists,
            ErrorNotExists::MuteNotExists>
handle(gut::constant<Operation::Unmute>, const struct Session &session,
       const Culture &culture);
