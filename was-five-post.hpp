#pragma once

#include "was-five-operations.hpp"
#include "was-five-status.hpp"

#include <chrono>
#include <map>
#include <unordered_map>
#include <vector>

using history_t =
    std::chrono::time_point<std::chrono::system_clock, std::chrono::minutes>;

struct Post {
  struct Id {
  } id;
  std::vector<struct Person> authors;
  struct Title {
  } title;
  struct Text {};
  std::map<history_t, Text> text; // 1 KiB.
  struct Tag {};
  std::vector<Tag> tags;
  struct Reference {};
  std::vector<Reference> references;

  struct Moderation {
    enum Category {
      // Bad
      EMBARASSING,  // Harmful to reputation or employment.
      UNSOURCED,    // Doesn't provide sources to back up claims of
                    // fact and evidence.
      INACCURATE,   // Incorrect, misinterpreted, misunderstood
                    // facts; disproven ideas; logically fallacious.
      DISINGENUOUS, // Misrepresented (cherry picking, omission,
                    // out of context), fabricated, fraudulent,
                    // vague, entitlement, malicious.
      // Good
      FUNNY,         // Elicited a smile, chuckle, laugh.
      ANALYTICAL,    // Well articulated discussion of points.
      PHILOSOPHICAL, // Reveals a deep concept or truth.
      FRESH,         // Uncommon point-of-view, framing or phrasing.
    };

    std::unordered_map<Category, int32_t> rating_count;

    struct Highlight {};
    std::vector<Highlight> highlights;
  } moderation;

  std::vector<Post> sources;
  std::vector<Post> responses;
};

// post.author banned session.person -> SessionNotAuthorized
SessionCode<SuccessfulOperation::PostCreated>
handle(gut::constant<Operation::ReadPost>, const struct Session &session,
       const Post &post);
SessionCode<SuccessfulOperation::PostCreated>
handle(gut::constant<Operation::MakePost>, const struct Session &session);
// culture banned session.person -> CultureNotAuthorized
SessionCode<SuccessfulOperation::PostCreated, ErrorNotExists::CultureNotExists,
            ErrorNotAuthorized::CultureNotAuthorized>
handle(gut::constant<Operation::MakePost>, const struct Session &session,
       const struct Culture &culture);
// culture banned session.person -> CultureNotAuthorized
SessionCode<SuccessfulOperation::EventCreated, ErrorNotExists::CultureNotExists,
            ErrorNotAuthorized::CultureNotAuthorized>
handle(gut::constant<Operation::MakeEvent>, const struct Session &session,
       const struct Culture &culture);
// post.author blocked session.person -> SessionNotAuthorized
// session.person blocked author -> PersonNotAuthorized
// session.person blocked culture -> CultureNotAuthorized
SessionCode<SuccessfulOperation::PostCreated, ErrorNotExists::PostNotExists,
            ErrorNotAuthorized::PersonNotAuthorized,
            ErrorNotAuthorized::CultureNotAuthorized>
handle(gut::constant<Operation::MakeResponse>, const struct Session &session,
       const Post &post);
// post.author blocked session.person -> SessionNotAuthorized
// session.person blocked author -> PersonNotAuthorized
// session.person blocked culture -> CultureNotAuthorized
SessionCode<SuccessfulOperation::PostRemoved, ErrorNotExists::PostNotExists,
            ErrorNotAuthorized::PersonNotAuthorized,
            ErrorNotAuthorized::CultureNotAuthorized>
handle(gut::constant<Operation::RemovePost>, const struct Session &session,
       const Post &post);
// post.author blocked session.person -> SessionNotAuthorized
// session.person blocked author -> PersonNotAuthorized
// session.person blocked culture -> CultureNotAuthorized
SessionCode<SuccessfulOperation::PostModified, ErrorNotExists::PostNotExists,
            ErrorNotAuthorized::PersonNotAuthorized,
            ErrorNotAuthorized::CultureNotAuthorized,
            ErrorNotAuthorized::TitleNotAuthorized>
handle(gut::constant<Operation::EditTitle>, const struct Session &session,
       const Post &post, const Post::Title &title);
// post.author blocked session.person -> SessionNotAuthorized
// session.person blocked author -> PersonNotAuthorized
// session.person blocked culture -> CultureNotAuthorized
SessionCode<SuccessfulOperation::PostModified, ErrorNotExists::PostNotExists,
            ErrorNotAuthorized::PersonNotAuthorized,
            ErrorNotAuthorized::CultureNotAuthorized,
            ErrorNotAuthorized::TextNotAuthorized>
handle(gut::constant<Operation::EditText>, const struct Session &session,
       const Post &post, const Post::Text &text);
// post.author blocked session.person -> SessionNotAuthorized
// session.person blocked author -> PersonNotAuthorized
// session.person blocked culture -> CultureNotAuthorized
SessionCode<SuccessfulOperation::PostModified, ErrorNotExists::PostNotExists,
            ErrorNotAuthorized::PersonNotAuthorized,
            ErrorNotAuthorized::CultureNotAuthorized,
            ErrorNotAuthorized::TagNotAuthorized>
handle(gut::constant<Operation::AddTag>, const struct Session &session,
       const Post &post, const Post::Tag &tag);
// post.author blocked session.person -> SessionNotAuthorized
// session.person blocked author -> PersonNotAuthorized
// session.person blocked culture -> CultureNotAuthorized
SessionCode<SuccessfulOperation::PostModified, ErrorNotExists::PostNotExists,
            ErrorNotAuthorized::PersonNotAuthorized,
            ErrorNotAuthorized::CultureNotAuthorized>
handle(gut::constant<Operation::RemoveTag>, const struct Session &session,
       const Post &post, const Post::Tag &tag);
// post.author blocked session.person -> SessionNotAuthorized
// session.person blocked author -> PersonNotAuthorized
// session.person blocked culture -> CultureNotAuthorized
SessionCode<SuccessfulOperation::PostModified, ErrorNotExists::PostNotExists,
            ErrorNotAuthorized::PersonNotAuthorized,
            ErrorNotAuthorized::CultureNotAuthorized,
            ErrorNotAuthorized::ReferenceNotAuthorized>
handle(gut::constant<Operation::AddReference>, const struct Session &session,
       const Post &post, const Post::Reference &reference);
// post.author blocked session.person -> SessionNotAuthorized
// session.person blocked author -> PersonNotAuthorized
// session.person blocked culture -> CultureNotAuthorized
SessionCode<SuccessfulOperation::PostModified, ErrorNotExists::PostNotExists,
            ErrorNotAuthorized::PersonNotAuthorized,
            ErrorNotAuthorized::CultureNotAuthorized>
handle(gut::constant<Operation::RemoveReference>, const struct Session &session,
       const Post &post, const Post::Reference &reference);

SessionCode<SuccessfulOperation::PostPublished, ErrorNotExists::PostNotExists,
            ErrorAlreadyExists::PublicationAlreadyExists>
handle(gut::constant<Operation::PublishPost>, const struct Session &session,
       const Post &post);
SessionCode<SuccessfulOperation::PostUnpublished, ErrorNotExists::PostNotExists,
            ErrorNotExists::PublicationNotExists>
handle(gut::constant<Operation::UnpublishPost>, const struct Session &session,
       const Post &post);

// reviewers blocked session.person -> SessionNotAuthorized
// post.culture banned session.person -> SessionNotAuthorized
// post.authors blocked session.person -> SessionNotAuthorized
// session.person blocked reviewers -> PersonNotAuthorized
// reviewers in post.authors -> PersonNotAuthorized
// reviewers not in session.person.friends -> PersonNotAuthorized
SessionCode<SuccessfulOperation::ReviewRequested, ErrorNotExists::PostNotExists,
            ErrorNotExists::PersonNotExists,
            ErrorNotAuthorized::PersonNotAuthorized>
handle(gut::constant<Operation::RequestReview>, const struct Session &session,
       const Post &post, const std::vector<Person> &reviewers);
// reviewers blocked session.person -> SessionNotAuthorized
// post.culture banned session.person -> SessionNotAuthorized
// post.authors blocked session.person -> SessionNotAuthorized
// session.person blocked post.authors -> PersonNotAuthorized
// reviewers in post.authors -> PersonNotAuthorized
// reviewers not in session.person.friends -> PersonNotAuthorized
SessionCode<SuccessfulOperation::ShareRequested, ErrorNotExists::PostNotExists,
            ErrorNotExists::PersonNotExists,
            ErrorNotAuthorized::PersonNotAuthorized>
handle(gut::constant<Operation::RequestShare>, const struct Session &session,
       const Post &post, const std::vector<Person> &reviewers);
// post.authors blocked session.person -> SessionNotAuthorized
// session.person blocked post.authors -> PersonNotAuthorized
SessionCode<SuccessfulOperation::PostModerated, ErrorNotExists::PostNotExists,
            ErrorNotAuthorized::PersonNotAuthorized>
handle(gut::constant<Operation::ModeratePost>, const struct Session &session,
       const Post &post, Post::Moderation::Category category);
// post.authors blocked session.person -> SessionNotAuthorized
// session.person blocked post.authors -> PersonNotAuthorized
SessionCode<SuccessfulOperation::PostModerated, ErrorNotExists::PostNotExists,
            ErrorNotAuthorized::PersonNotAuthorized>
handle(gut::constant<Operation::ModerateHighlight>,
       const struct Session &session, const Post &post,
       Post::Moderation::Highlight highlight,
       Post::Moderation::Category category);
