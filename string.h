#pragma once

#include <cstddef>

template <typename... ArgTypes> const void *memchr(ArgTypes &&...);
template <typename... ArgTypes> void memcmp(ArgTypes &&...);
extern "C" void *memcpy(void *dest, const void *src, std::size_t count);
extern "C" void *memmove(void *dest, const void *src, std::size_t count);
extern "C" void *memset(void *dest, int c, std::size_t count);
template <typename... ArgTypes> void strcat(ArgTypes &&...);
template <typename... ArgTypes> const char *strchr(ArgTypes &&...);
template <typename... ArgTypes> void strcmp(ArgTypes &&...);
template <typename... ArgTypes> void strcoll(ArgTypes &&...);
template <typename... ArgTypes> void strcpy(ArgTypes &&...);
template <typename... ArgTypes> void strcspn(ArgTypes &&...);
template <typename... ArgTypes> void strerror(ArgTypes &&...);
extern "C" std::size_t strlen(const char *);
template <typename... ArgTypes> void strncat(ArgTypes &&...);
template <typename... ArgTypes> void strncmp(ArgTypes &&...);
template <typename... ArgTypes> void strncpy(ArgTypes &&...);
template <typename... ArgTypes> const char *strpbrk(ArgTypes &&...);
template <typename... ArgTypes> const char *strrchr(ArgTypes &&...);
template <typename... ArgTypes> void strspn(ArgTypes &&...);
template <typename... ArgTypes> const char *strstr(ArgTypes &&...);
template <typename... ArgTypes> void strtok(ArgTypes &&...);
template <typename... ArgTypes> void strxfrm(ArgTypes &&...);
