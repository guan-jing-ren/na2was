#pragma once

#include <sqlite3.h>

#include <ntuple.hpp>
#include <reflect.hpp>
#include <resource.hpp>
#include <variant.hpp>

#include <filesystem>
#include <string>
#include <unordered_map>
#include <vector>

using column_value =
    gut::variant<int64_t, double, std::string, std::vector<unsigned char>>;

using result_row = std::unordered_map<std::string, column_value>;

struct SQLite
    : gut::resource<sqlite3_close_v2, static_cast<sqlite3 *>(nullptr)> {
  struct InternalString
      : private gut::resource<sqlite3_free, static_cast<char *>(nullptr)> {
  private:
    friend struct SQLite;
    InternalString(char *);
    operator const char *() const;
  };

  struct PreparedStatement
      : gut::resource<sqlite3_finalize, static_cast<sqlite3_stmt *>(nullptr)> {

    PreparedStatement(const SQLite &db, std::string_view sql);

    gut::optional<std::vector<result_row>>
    operator()(std::unordered_map<std::string, column_value> = {}) const;

  private:
    gut::ref<const SQLite &> m_db;
  };

  struct Transaction {
    ~Transaction();

  private:
    friend struct SQLite;

    gut::ref<const SQLite &> m_db;
    gut::ref<const PreparedStatement &> m_rollback;
    gut::ref<const PreparedStatement &> m_commit;

    Transaction(const SQLite &db, const PreparedStatement &rollback,
                const PreparedStatement &commit);
  };

  struct Blob : gut::resource<sqlite3_blob_close,
                              static_cast<sqlite3_blob *>(nullptr)> {};

  struct Backup : gut::resource<sqlite3_backup_finish,
                                static_cast<sqlite3_backup *>(nullptr)> {};

  template <typename ColumnType> struct Column : gut::optional<ColumnType> {
    using base_type = gut::optional<ColumnType>;
    using base_type::base_type;
    constexpr static auto column_type = gut::type_v<ColumnType>;
  };

  template <typename ColumnType> struct PrimaryKey : Column<ColumnType> {};

  template <auto ForeignColumn>
  struct ColumnReference
      : Column<typename decltype(
            gut::type_v<decltype(ForeignColumn)>.remove_member_pointer().construct().column_type)::
                   raw_type> {
    using base_type = Column<typename decltype(
        gut::type_v<decltype(ForeignColumn)>.remove_member_pointer().construct().column_type)::
                                 raw_type>;

    using base_type::base_type;
  };

  template <auto ForeignColumn>
  struct ForeignKey : ColumnReference<ForeignColumn> {
    using base_type = ColumnReference<ForeignColumn>;

    template <typename TableType>
    ForeignKey(const TableType &table) : base_type(table.*ForeignColumn) {}

  private:
    friend struct Session;
    using base_type::base_type;
  };

  enum OpenMode {
    ReadOnly = SQLITE_OPEN_READONLY,
    ReadWrite = SQLITE_OPEN_READWRITE,
    Create = SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE
  };

  enum NameAttributes {
    None = 0,
    URI = SQLITE_OPEN_URI,
    Memory = SQLITE_OPEN_MEMORY,
    NoMutex = SQLITE_OPEN_NOMUTEX
  };

  SQLite(const std::filesystem::path &dbfile, OpenMode mode = Create,
         NameAttributes attr = NoMutex);
  SQLite(SQLite &&) = default;
  ~SQLite();

  template <typename Namespace, typename Operation,
            typename Rows = gut::something>
  void
  operator()(const Namespace &ns, const PreparedStatement &stmt,
             const Operation &operation,
             gut::optional<gut::ref<std::vector<Rows> &>> rows = {}) const {
    static_assert(gut::is_reflect_namespace_v<Namespace>);
    std::unordered_map<std::string, column_value> parameters;
    auto get_parameters = [&parameters, &ns, &operation](auto member_and_name) {
      parameters.emplace(
          ("@" + member_and_name.get(gut::index_v<1>)).to_buffer().data(),
          [&ns](auto &&value) {
            constexpr auto value_type =
                gut::type_v<decltype(value)>.remove_cvref();
            if constexpr (value_type.is_enum())
              return ns.member(value_type)
                  .members_and_names()
                  .map([value](auto &&member_and_name) {
                    if (value == member_and_name.get(gut::index_v<0>))
                      return std::string{member_and_name.get(gut::index_v<1>)
                                             .to_buffer()
                                             .data()};
                    return std::string{};
                  })
                  .reduce([](auto &&... value) { return (... + value); });
            else
              return value;
          }((operation.*member_and_name.get(gut::index_v<0>).value).value()));
    };
    flatten_members(ns, ns.member(gut::type_v<Operation>), get_parameters);
    [[maybe_unused]] auto result = stmt(move(parameters));
    if constexpr (!gut::is_something_v<Rows>)
      if (result && rows) {
        rows.value().get().resize(result.value().size());
        auto get_rows = [&rows = rows.value().get(), &result = result.value(),
                         &ns](auto member_and_name) {
          for (size_t first = 0, last = result.size(); first != last; ++first)
            result[first]
                  [member_and_name.get(gut::index_v<1>).to_buffer().data()]
                      .each([&rows, first, member_and_name,
                             &ns](auto &&result_column) {
                        constexpr auto result_column_type =
                            gut::type_v<decltype(result_column)>.remove_cvref();
                        constexpr auto out_column_type = gut::type_v<decltype((rows[first].*
                              member_and_name.get(gut::index_v<0>).value).value())>.remove_cvref();
                        if constexpr (result_column_type == out_column_type)
                          rows[first].*
                              member_and_name.get(gut::index_v<0>).value =
                              [out_column_type, &ns](auto &&value) {
                                (void)ns;
                                if constexpr (out_column_type.is_enum())
                                  ns.member(out_column_type)
                                      .members_and_names()
                                      .map([value](auto &&member_and_name) {
                                        if (value == member_and_name.get(
                                                         gut::index_v<1>))
                                          return member_and_name.get(
                                              gut::index_v<0>);
                                        return out_column_type.construct();
                                      })
                                      .reduce([](auto &&... value) {
                                        return (... + value);
                                      });
                                else
                                  return move(value);
                              }(move(result_column));
                      });
        };
        flatten_members(ns, ns.member(gut::type_v<Rows>), get_rows);
      }
  }

  void transact(const std::function<void(const SQLite &)> &scope_guard) const;

  void optimize() const;

private:
  gut::optional<PreparedStatement> m_optimize;
  gut::optional<PreparedStatement> m_rollback;
  gut::optional<PreparedStatement> m_begin;
  gut::optional<PreparedStatement> m_commit;

  SQLite();

  template <auto SQLiteFunc, bool Timed = false, typename... Args>
  gut::variant<gut::ntuple<int, int>, std::vector<result_row>>
  call(Args... args) const;

  template <typename Namespace, typename Reflected, typename Func>
  void flatten_members(const Namespace &ns, const Reflected &reflected,
                       Func &f) const {
    if constexpr (auto bases = reflected.bases(); bases.count() > 0)
      bases.map([this, &ns, &f](auto base) {
        const auto reflect_base = ns.member(base);
        this->flatten_members(ns, reflect_base, f);
        reflect_base.members_and_names().each(f);
      });
    if constexpr (Reflected::count() > 0)
      reflected.members_and_names().each(f);
  }
};
