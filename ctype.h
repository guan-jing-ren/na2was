#pragma once

template <typename... Args> void isalnum(Args &&...);
template <typename... Args> void isalpha(Args &&...);
template <typename... Args> void isblank(Args &&...);
template <typename... Args> void iscntrl(Args &&...);
template <typename... Args> void isdigit(Args &&...);
template <typename... Args> void isgraph(Args &&...);
template <typename... Args> void islower(Args &&...);
template <typename... Args> void isprint(Args &&...);
template <typename... Args> void ispunct(Args &&...);
template <typename... Args> void isspace(Args &&...);
template <typename... Args> void isupper(Args &&...);
template <typename... Args> void isxdigit(Args &&...);
template <typename... Args> void tolower(Args &&...);
template <typename... Args> void toupper(Args &&...);
