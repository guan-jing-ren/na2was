#pragma once

#include "was.hpp"

struct DOMString : JsString {
  DOMString(int32_t);
  DOMString(double);
protected:
  friend struct JsRef;
  using JsString::JsString;
};

struct DOMNamespace : DOMString {
  static DOMNamespace HTML();
  static DOMNamespace MathML();
  static DOMNamespace SVG();
  static DOMNamespace XLink();
  static DOMNamespace XML();
  static DOMNamespace XMLNS();

protected:
  friend struct JsRef;
  using DOMString::DOMString;
};

struct Range : JsRef {
private:
  friend struct JsRef;
  using JsRef::JsRef;
};

struct EventInit : JsRef {
  EventInit(bool bubbles = false, bool cancelable = false);

protected:
  using JsRef::JsRef;
};

struct Event : JsRef {
  static constexpr unsigned short NONE = 0;
  static constexpr unsigned short CAPTURING_PHASE = 1;
  static constexpr unsigned short AT_TARGET = 2;
  static constexpr unsigned short BUBBLING_PHASE = 3;

  DOMString type() const;
  gut::optional<struct EventTarget> target() const;
  gut::optional<struct EventTarget> currentTarget() const;

  unsigned short eventPhase() const;

  void stopPropagation() const;
  void stopImmediatePropagation() const;

  bool bubbles() const;
  bool cancelable() const;
  void preventDefault() const;
  bool defaultPrevented() const;

  bool isTrusted() const;
  double timeStamp() const;

  Event(const DOMString &type, EventInit = {});

protected:
  friend struct JsRef;
  friend struct CustomEvent;
  using JsRef::JsRef;
  Event(const char *custom, const DOMString &type, EventInit);
};

struct CustomEventInit : EventInit {
  CustomEventInit(EventInit = {}, gut::optional<::JsRef> detail = {});

private:
  using EventInit::property;
};

struct CustomEvent : Event {
  JsRef detail() const;

  CustomEvent(const DOMString &type, CustomEventInit);
};

struct EventTarget : JsRef {
  template <typename Callback>
  void addEventListener(const DOMString &type, Callback callback,
                        bool capture = false) const {
    addEventListener(type, static_cast<void (*)(JsRef)>(callback), capture);
  }
  void addEventListener(const DOMString &type, void (*callback)(JsRef),
                        bool capture = false) const;
  template <typename Callback>
  void removeEventListener(const DOMString &type, Callback callback,
                           bool capture = false) const {
    removeEventListener(type, static_cast<void (*)(JsRef)>(callback), capture);
  }
  void removeEventListener(const DOMString &type, void (*callback)(JsRef),
                           bool capture = false) const;
  bool dispatchEvent(const Event &event) const;

protected:
  friend struct JsRef;
  template <typename> friend struct ChildNode;
  using JsRef::JsRef;
};

struct Node : EventTarget {
  DOMString nodeName() const;
  gut::optional<struct Document> ownerDocument() const;
  gut::optional<Node> parentNode() const;
  gut::optional<struct Element> parentElement() const;
  bool hasChildNodes() const;
  gut::optional<DOMString> textContent() const;
  void normalize() const;
  Node cloneNode(bool deep = false) const;
  bool contains(const Node &other) const;
  Node insertBefore(const Node &node, const Node &child) const;
  Node appendChild(const Node &node) const;
  Node replaceChild(const Node &node, const Node &child) const;
  Node removeChild(const Node &child) const;

protected:
  friend struct JsRef;
  using EventTarget::EventTarget;
};

template <typename List> struct JsDomInternalList {
protected:
  template <typename Each> void each(Each &&each) {
    for (auto i = 0ul, last = static_cast<const List *>(this)->length();
         i != last; ++i)
      static_cast<const List *>(this)->item(i).each(each);
  }
};

struct NodeList : JsRef, private JsDomInternalList<NodeList> {
  gut::optional<Node> item(unsigned long index) const;
  unsigned long length() const;

  using JsDomInternalList::each;

protected:
  friend struct JsRef;
  friend struct JsDomInternalList;
  using JsRef::JsRef;
};

struct ParentNode : Node {
  gut::optional<struct Element> querySelector(const char *) const;
  gut::optional<struct NodeList> querySelectorAll(const char *) const;

protected:
  explicit ParentNode(Node n);
  using Node::Node;
};

template <typename NodeType> struct ChildNode { void remove(); };

struct DocumentType : Node {
  DOMString name() const;
  DOMString publicId() const;
  DOMString systemId() const;

private:
  friend struct JsRef;
  using Node::Node;
};

struct DOMImplementation : JsRef {
  DocumentType createDocumentType(const DOMString &qualifiedName,
                                  const DOMString &publicId,
                                  const DOMString &systemId) const;
  struct XMLDocument
  createDocument(const DOMString &qualifiedName, const DOMString &ns = {},
                 gut::optional<DocumentType> doctype = {}) const;
  Document createHTMLDocument(const DOMString &title = {}) const;

private:
  friend struct JsRef;
  using JsRef::JsRef;
};

struct DocumentFragment : Node {
private:
  friend struct JsRef;
  using Node::Node;
};

struct Document : ParentNode {
  DOMImplementation implementation() const;
  Element documentElement() const;
  Element createElementNS(const DOMString &qualifiedName,
                          const DOMString &ns = {}) const;
  struct Text createTextNode(const DOMString &data) const;
  DocumentFragment createDocumentFragment() const;
  Node importNode(const Node &node, bool deep = false) const;
  Node adoptNode(const Node &node) const;
  Event createEvent(const DOMString &interface) const;
  Range createRange() const;

protected:
  friend struct JsRef;
  using ParentNode::ParentNode;
};

struct XMLDocument : Document {
private:
  friend struct JsRef;
  using Document::Document;
};

struct Text : Node {
  DOMString data() const;
  void data(const DOMString &data) const;
  unsigned long length() const;
  DOMString substringData(unsigned long offset, unsigned long count) const;
  void appendData(const DOMString &data) const;
  void insertData(unsigned long offset, const DOMString &data) const;
  void deleteData(unsigned long offset, unsigned long count) const;
  void replaceData(unsigned long offset, unsigned long count,
                   const DOMString &data) const;
  Text splitText(unsigned long offset) const;
  DOMString wholeText() const;

private:
  friend struct JsRef;
  using Node::Node;
};

struct DOMTokenList : JsRef, private JsDomInternalList<DOMTokenList> {
  unsigned long length() const;
  gut::optional<DOMString> item(unsigned long index) const;
  bool contains(const DOMString &token) const;
  void add(std::initializer_list<DOMString> tokens) const;
  void remove(std::initializer_list<DOMString> tokens) const;
  bool toggle(const DOMString &token, bool force = false) const;

  using JsDomInternalList::each;

private:
  friend struct JsRef;
  friend struct JsDomInternalList;
  using JsRef::JsRef;
};

// Just reusing node list, but is not a node list, hence private.
struct NamedNodeMap : private NodeList {
  using NodeList::each;
  using NodeList::item;
  using NodeList::length;

private:
  friend struct JsRef;
  using NodeList::NodeList;
};

struct Element : ParentNode, ChildNode<Element> {
  explicit Element(Node);

  DOMString tagName() const;
  DOMString id() const;
  void id(const DOMString &id) const;
  DOMString className() const;
  void className(const DOMString &name) const;
  DOMTokenList classList() const;
  NamedNodeMap attributes() const;
  gut::optional<DOMString> getAttributeNS(const DOMString &localName,
                                          const DOMString &ns = {}) const;
  void setAttributeNS(const DOMString &name, const DOMString &value,
                      const DOMString &ns = {}) const;
  void removeAttributeNS(const DOMString &localName,
                         const DOMString &ns = {}) const;
  bool hasAttributeNS(const DOMString &localName,
                      const DOMString &ns = {}) const;

private:
  friend struct JsRef;
  using ParentNode::ParentNode;
};
