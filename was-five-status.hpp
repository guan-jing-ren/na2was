#pragma once

#include <variant.hpp>

enum class ErrorMalformedArguments { MalformedArguments };

enum class SuccessfulOperation {
  PersonRegistered,
  PersonDeregistered,
  PersonModified,
  PersonRelated,
  PersonUnrelated,
  PersonParticipated,
  PersonUnparticipated,
  PersonBlocked,
  PersonUnBlocked,
  PostMuted,
  BidPlaced,
  BidDropped,
  BidModified,
  SessionStarted,
  SessionEnded,
  CultureStarted,
  CultureDismantled,
  CultureModified,
  OrganizerAdded,
  OrganizerRetired,
  ModeratorAdded,
  ModeratorRetired,
  ParticipantAdded,
  ParticipantRemoved,
  ParticipantBanned,
  PostCreated,
  EventCreated,
  PostRemoved,
  PostModified,
  PostPublished,
  PostUnpublished,
  ReviewRequested,
  ShareRequested,
  PostModerated,
  AdvertReported,
  PersonReported,
  CultureReported,
  PostReported,
};

enum class ErrorAlreadyExists {
  PersonAlreadyExists,
  SessionAlreadyExists,
  RelationAlreadyExists,
  ParticipationAlreadyExists,
  BlockAlreadyExists,
  MuteAlreadyExists,
  AdvertAlreadyExists,
  BidAlreadyExists,
  CultureAlreadyExists,
  OrganizerAlreadyExists,
  ModeratorAlreadyExists,
  ParticipantAlreadyExists,
  PublicationAlreadyExists,
  ReportAlreadyExists,
};

enum class ErrorNotExists {
  PersonNotExists,
  SessionNotExists,
  RelationNotExists,
  ParticipationNotExists,
  BlockNotExists,
  MuteNotExists,
  AdvertNotExists,
  BidNotExists,
  CultureNotExists,
  OrganizerNotExists,
  ModeratorNotExists,
  ParticipantNotExists,
  PostNotExists,
  PublicationNotExists,
};

enum class ErrorNotAuthorized {
  SessionNotAuthorized,
  PersonNotAuthorized,
  CultureNotAuthorized,
  AdvertNotAuthorized,
  TitleNotAuthorized,
  TextNotAuthorized,
  TagNotAuthorized,
  ReferenceNotAuthorized,
};

template <auto... Codes>
using ReturnCode = gut::variant<gut::constant<Codes>...>;

template <auto SuccessCode, auto... ErrorCodes>
using RemoteCode =
    ReturnCode<SuccessCode, ErrorMalformedArguments::MalformedArguments,
               ErrorCodes...>;

// session.person banned from site, session expired -> SessionNotAuthorized
template <auto SuccessCode, auto... ErrorCodes>
using SessionCode =
    RemoteCode<SuccessCode, ErrorNotAuthorized::SessionNotAuthorized,
               ErrorCodes...>;
