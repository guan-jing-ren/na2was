#pragma once

#include "was-five-post.hpp"

#include <string>

struct Culture {
  struct Id {
  } id;
  std::string name;    // 25 characters.
  std::string tagline; // 75 characters.
  std::string mission; // 100 words.
  std::vector<struct Person> organizers,
      moderators /* At least 1 per 20 people */, participants;
  std::vector<struct Person> banned;
  std::vector<Post> posts;

  struct Event : Post {
    history_t when;
    std::string where;
  };
  std::vector<Event> events;
};

bool operator==(const Culture&,const Culture&);

SessionCode<SuccessfulOperation::CultureStarted,
            ErrorAlreadyExists::CultureAlreadyExists>
handle(gut::constant<Operation::StartCulture>, const Session &session,
       const std::string &name);
// session.person not in culture.organizer or in culture.banned ->
// SessionNotAuthorized
SessionCode<SuccessfulOperation::CultureDismantled,
            ErrorNotExists::CultureNotExists>
handle(gut::constant<Operation::DismantleCulture>, const Session &session,
       const Culture &culture);
// session.person not in culture.organizer or in culture.banned ->
// SessionNotAuthorized
SessionCode<SuccessfulOperation::CultureModified,
            ErrorNotExists::CultureNotExists>
handle(gut::constant<Operation::ChangeTagline>, const Session &session,
       const Culture &culture, const std::string &tagline);
// session.person not in culture.organizer or in culture.banned ->
// SessionNotAuthorized
SessionCode<SuccessfulOperation::CultureModified,
            ErrorNotExists::CultureNotExists>
handle(gut::constant<Operation::ChangeMission>, const Session &session,
       const Culture &culture, const std::string &mission);
// organizer blocked session.person -> SessionNotAuthorized
// organizer blocked culture -> CultureNotAuthorized
// culture banned moderator -> PersonNotAuthorized
SessionCode<
    SuccessfulOperation::OrganizerAdded, ErrorNotExists::CultureNotExists,
    ErrorNotAuthorized::CultureNotAuthorized, ErrorNotExists::PersonNotExists,
    ErrorNotAuthorized::PersonNotAuthorized,
    ErrorAlreadyExists::OrganizerAlreadyExists>
handle(gut::constant<Operation::AddOrganizer>, const Session &session,
       const Culture &culture, const Person &organizer);
// Unilateral
SessionCode<SuccessfulOperation::OrganizerRetired,
            ErrorNotExists::CultureNotExists, ErrorNotExists::PersonNotExists,
            ErrorNotExists::OrganizerNotExists>
handle(gut::constant<Operation::RetireOrganizer>, const Session &session,
       const Culture &culture, const Person &organizer);
// moderator blocked session.person -> SessionNotAuthorized
// moderator blocked culture -> CultureNotAuthorized
// culture banned moderator -> PersonNotAuthorized
SessionCode<
    SuccessfulOperation::ModeratorAdded, ErrorNotExists::CultureNotExists,
    ErrorNotAuthorized::CultureNotAuthorized, ErrorNotExists::PersonNotExists,
    ErrorNotAuthorized::PersonNotAuthorized,
    ErrorAlreadyExists::ModeratorAlreadyExists>
handle(gut::constant<Operation::AddModerator>, const Session &session,
       const Culture &culture, const Person &moderator);
// Unilateral
SessionCode<SuccessfulOperation::ModeratorRetired,
            ErrorNotExists::CultureNotExists, ErrorNotExists::PersonNotExists,
            ErrorNotExists::ModeratorNotExists>
handle(gut::constant<Operation::RetireModerator>, const Session &session,
       const Culture &culture, const Person &moderator);
// participant blocked session.person -> SessionNotAuthorized
// participant blocked culture -> CultureNotAuthorized
// culture banned moderator -> PersonNotAuthorized
SessionCode<
    SuccessfulOperation::ParticipantAdded, ErrorNotExists::CultureNotExists,
    ErrorNotAuthorized::CultureNotAuthorized, ErrorNotExists::PersonNotExists,
    ErrorNotAuthorized::PersonNotAuthorized,
    ErrorAlreadyExists::ParticipantAlreadyExists>
handle(gut::constant<Operation::AddParticipant>, const Session &session,
       const Culture &culture, const Person &participant);
// Unilateral
SessionCode<SuccessfulOperation::ParticipantRemoved,
            ErrorNotExists::CultureNotExists, ErrorNotExists::PersonNotExists,
            ErrorNotExists::ParticipantNotExists>
handle(gut::constant<Operation::RemoveParticipant>, const Session &session,
       const Culture &culture, const Person &participant);
// Unilateral
SessionCode<SuccessfulOperation::ParticipantBanned,
            ErrorNotExists::CultureNotExists, ErrorNotExists::PersonNotExists,
            ErrorNotExists::ParticipantNotExists>
handle(gut::constant<Operation::BanParticipant>, const Session &session,
       const Culture &culture, const Person &participant);
