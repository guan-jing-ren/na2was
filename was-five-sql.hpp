#pragma once

#include "was-sql.hpp"

#include <optional.hpp>
#include <reflect.hpp>

struct Site {
  struct SIGNUPS {
    SQLite::PrimaryKey<int64_t> ID;
    SQLite::Column<std::string> EMAIL;
    enum class Access { BANNED, PENDING, REGISTERED, REMOVED };
    SQLite::Column<Access> ACCESS;
  };

  struct PEOPLE {
    SQLite::ForeignKey<&SIGNUPS::ID> ID;
    SQLite::Column<std::string> HANDLE;
    SQLite::Column<std::vector<unsigned char>> AVATAR;
    SQLite::Column<std::vector<unsigned char>> CREDENTIAL;
    enum class Membership { FREE, SUBSIDIZED, PAID };
    SQLite::Column<Membership> MEMBERSHIP;
  };

  struct CULTURES {
    SQLite::PrimaryKey<int64_t> ID;
    SQLite::Column<std::string> CULTURE;
    SQLite::Column<std::string> TAGLINE;
    SQLite::Column<std::string> MISSION;
  };

  struct POSTS {
    SQLite::PrimaryKey<int64_t> ID;
    SQLite::Column<int64_t> CREATED;
    SQLite::Column<int64_t> MODIFIED;
    SQLite::Column<std::string> TITLE;
    SQLite::Column<std::string> CONTENT;
  };

  struct AUTHORS {
    SQLite::ForeignKey<&PEOPLE::ID> AUTHOR;
    SQLite::ForeignKey<&POSTS::ID> POST;
  };

  struct RESPONSES {
    SQLite::ForeignKey<&POSTS::ID> POST;
    SQLite::ForeignKey<&POSTS::ID> RESPONSE;
  };

  struct PARTICIPANTS {
    SQLite::ForeignKey<&CULTURES::ID> CULTURE;
    SQLite::ForeignKey<&PEOPLE::ID> PARTICIPANT;
    enum class Standing { BANNED, PARTICIPANT, MODERATOR, ORGANIZER };
    SQLite::Column<Standing> STANDING;
  };

  struct PUBLISHED {
    SQLite::ForeignKey<&CULTURES::ID> CULTURE;
    SQLite::ForeignKey<&POSTS::ID> POST;
  };

  struct BLOCKED_CULTURES {
    SQLite::ForeignKey<&PEOPLE::ID> PERSON;
    SQLite::ForeignKey<&CULTURES::ID> CULTURE;
  };

  struct RELATIONSHIPS {
    SQLite::ForeignKey<&PEOPLE::ID> PERSON1;
    SQLite::ForeignKey<&PEOPLE::ID> PERSON2;
    enum class Relation {
      BLOCKED,
      CONTACT,
      COLLEAGUE,
      FRIEND,
      FAMILY,
      TRUSTED
    };
    SQLite::Column<Relation> RELATION;
  };

  struct MUTED_POSTS {
    SQLite::ForeignKey<&PEOPLE::ID> PERSON;
    SQLite::ForeignKey<&POSTS::ID> POST;
  };

  struct MUTED_PEOPLE {
    SQLite::ForeignKey<&PEOPLE::ID> PERSON1;
    SQLite::ForeignKey<&PEOPLE::ID> PERSON2;
  };

  struct MUTED_CULTURES {
    SQLite::ForeignKey<&PEOPLE::ID> PERSON;
    SQLite::ForeignKey<&CULTURES::ID> CULTURE;
  };

  struct ChangeEmail {
    SQLite::ColumnReference<&SIGNUPS::ID> SID;
    SQLite::ColumnReference<&SIGNUPS::EMAIL> EMAIL;
  };

  struct ChangeAccess {
    SQLite::ColumnReference<&SIGNUPS::ID> SID;
    SQLite::ColumnReference<&SIGNUPS::ACCESS> ACCESS;
  };

  struct NewSignup {
    SQLite::ColumnReference<&SIGNUPS::EMAIL> EMAIL;
  };

  struct AddPerson {
    SQLite::ColumnReference<&SIGNUPS::ID> ID;
  };

  Site(SQLite &&db);

  void operator()(const ChangeEmail &) const;
  void operator()(const ChangeAccess &) const;
  void operator()(const NewSignup &) const;
  void operator()(const AddPerson &) const;

private:
  SQLite m_db;
  gut::optional<SQLite::PreparedStatement> m_change_email;
  gut::optional<SQLite::PreparedStatement> m_change_access;
  gut::optional<SQLite::PreparedStatement> m_new_signup;
  gut::optional<SQLite::PreparedStatement> m_add_person;

  void site_tables() const;
  void site_operations();
};

struct Session {
  Session(SQLite &&db, int64_t person_id);

  struct CultureIdBase {
    SQLite::ColumnReference<&Site::CULTURES::ID> CID;
  };

  struct PostBase {
    SQLite::ColumnReference<&Site::POSTS::ID> PID;
  };

  struct ModifyPost : PostBase {
    SQLite::ColumnReference<&Site::POSTS::MODIFIED> MODIFIED =
        std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch())
            .count();
  };

  struct ParticipantBase {
    SQLite::ColumnReference<&Site::CULTURES::ID> CID;
    SQLite::ColumnReference<&Site::PEOPLE::ID> PID;
  };

  struct CultureBase {
    SQLite::ColumnReference<&Site::CULTURES::ID> CULTURE;
  };

  struct Person2Base {
    SQLite::ColumnReference<&Site::PEOPLE::ID> PERSON2;
  };

  struct MutePostBase {
    SQLite::ColumnReference<&Site::POSTS::ID> POST;
  };

  struct PERPEOPLE {
    SQLite::ColumnReference<&Site::PEOPLE::ID> PERSON;
  };

  struct PERCULTURE {
    SQLite::ColumnReference<&Site::CULTURES::ID> CULTURE;
  };

  struct PERSTREAM {
    SQLite::ColumnReference<&Site::POSTS::ID> CONTENT;
  };

  struct ChangeHandle {
    SQLite::ColumnReference<&Site::PEOPLE::HANDLE> HANDLE;
  };

  struct ChangeCredential {
    SQLite::ColumnReference<&Site::PEOPLE::CREDENTIAL> CREDENTIAL;
  };

  struct ChangeMembership {
    SQLite::ColumnReference<&Site::PEOPLE::MEMBERSHIP> MEMBERSHIP;
  };

  struct ChangeCulture : CultureIdBase {
    SQLite::ColumnReference<&Site::CULTURES::CULTURE> CULTURE;
  };

  struct ChangeTagline : CultureIdBase {
    SQLite::ColumnReference<&Site::CULTURES::TAGLINE> TAGLINE;
  };

  struct ChangeMission : CultureIdBase {
    SQLite::ColumnReference<&Site::CULTURES::MISSION> MISSION;
  };

  struct EndCulture : CultureIdBase {};

  struct ModifyTitle : ModifyPost {
    SQLite::ColumnReference<&Site::POSTS::TITLE> TITLE;
  };

  struct ModifyContent : ModifyPost {
    SQLite::ColumnReference<&Site::POSTS::CONTENT> CONTENT;
  };

  struct RemovePost : PostBase {};

  struct AddAuthor : Site::AUTHORS {};

  struct RemoveAuthor : Site::AUTHORS {};

  struct AddResponse : Site::RESPONSES {};

  struct AddParticipant : ParticipantBase {};

  struct RemoveParticipant : ParticipantBase {};

  struct ChangeStanding : ParticipantBase {
    SQLite::ColumnReference<&Site::PARTICIPANTS::STANDING> STANDING;
  };

  struct PublishPost : Site::PUBLISHED {};

  struct UnpublishPost : Site::PUBLISHED {};

  struct BlockCulture : CultureBase {};

  struct UnblockCulture : CultureBase {};

  struct AddRelation : Person2Base {};

  struct RemoveRelation : Person2Base {};

  struct ChangeRelation : Person2Base {
    SQLite::ColumnReference<&Site::RELATIONSHIPS::RELATION> RELATION;
  };

  struct MutePost : MutePostBase {};

  struct UnmutePost : MutePostBase {};

  struct MutePerson : Person2Base {};

  struct UnmutePerson : Person2Base {};

  struct MuteCulture : CultureBase {};

  struct UnmuteCulture : CultureBase {};

  struct NewPost {
    SQLite::ColumnReference<&Site::POSTS::TITLE> TITLE;
    SQLite::ColumnReference<&Site::POSTS::CONTENT> CONTENT;

    SQLite::ColumnReference<&Site::POSTS::CREATED> CREATED =
        std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch())
            .count();
  };

  struct NewCulture {};

  struct ReadStream {};

  struct ReadAuthor {
    SQLite::ColumnReference<&Site::PEOPLE::ID> AUTHOR;
  };

  struct ReadCulture {
    SQLite::ColumnReference<&Site::CULTURES::ID> CULTURE;
  };

  struct PostContent {
    SQLite::ColumnReference<&Site::POSTS::CONTENT> CONTENT;
  };

  void operator()(const ChangeHandle &) const;
  void operator()(const ChangeCredential &) const;
  void operator()(const ChangeMembership &) const;
  void operator()(const ChangeCulture &) const;
  void operator()(const ChangeTagline &) const;
  void operator()(const ChangeMission &) const;
  void operator()(const EndCulture &) const;
  void operator()(const ModifyTitle &) const;
  void operator()(const ModifyContent &) const;
  void operator()(const RemovePost &) const;
  void operator()(const AddAuthor &) const;
  void operator()(const RemoveAuthor &) const;
  void operator()(const AddResponse &) const;
  void operator()(const AddParticipant &) const;
  void operator()(const RemoveParticipant &) const;
  void operator()(const ChangeStanding &) const;
  void operator()(const PublishPost &) const;
  void operator()(const UnpublishPost &) const;
  void operator()(const BlockCulture &) const;
  void operator()(const UnblockCulture &) const;
  void operator()(const AddRelation &) const;
  void operator()(const RemoveRelation &) const;
  void operator()(const ChangeRelation &) const;
  void operator()(const MutePost &) const;
  void operator()(const UnmutePost &) const;
  void operator()(const MutePerson &) const;
  void operator()(const UnmutePerson &) const;
  void operator()(const MuteCulture &) const;
  void operator()(const UnmuteCulture &) const;

  void operator()(const NewPost &) const;
  void operator()(const NewCulture &) const;
  std::vector<PostContent> operator()(const ReadStream &) const;
  std::vector<PostContent> operator()(const ReadAuthor &) const;
  std::vector<PostContent> operator()(const ReadCulture &) const;

private:
  SQLite m_db;
  int64_t m_person_id;
  gut::optional<SQLite::PreparedStatement> m_post_by_author;
  gut::optional<SQLite::PreparedStatement> m_publish_by_culture;
  gut::optional<SQLite::PreparedStatement> m_post_stream;

  gut::optional<SQLite::PreparedStatement> m_change_handle;
  gut::optional<SQLite::PreparedStatement> m_change_credential;
  gut::optional<SQLite::PreparedStatement> m_change_membership;
  gut::optional<SQLite::PreparedStatement> m_change_culture;
  gut::optional<SQLite::PreparedStatement> m_change_tagline;
  gut::optional<SQLite::PreparedStatement> m_change_mission;
  gut::optional<SQLite::PreparedStatement> m_end_culture;
  gut::optional<SQLite::PreparedStatement> m_modify_title;
  gut::optional<SQLite::PreparedStatement> m_modify_content;
  gut::optional<SQLite::PreparedStatement> m_remove_post;
  gut::optional<SQLite::PreparedStatement> m_add_author;
  gut::optional<SQLite::PreparedStatement> m_remove_author;
  gut::optional<SQLite::PreparedStatement> m_add_response;
  gut::optional<SQLite::PreparedStatement> m_add_participant;
  gut::optional<SQLite::PreparedStatement> m_remove_participant;
  gut::optional<SQLite::PreparedStatement> m_change_standing;
  gut::optional<SQLite::PreparedStatement> m_publish_post;
  gut::optional<SQLite::PreparedStatement> m_unpublish_post;
  gut::optional<SQLite::PreparedStatement> m_block_culture;
  gut::optional<SQLite::PreparedStatement> m_unblock_culture;
  gut::optional<SQLite::PreparedStatement> m_add_relation;
  gut::optional<SQLite::PreparedStatement> m_remove_relation;
  gut::optional<SQLite::PreparedStatement> m_change_relation;
  gut::optional<SQLite::PreparedStatement> m_mute_post;
  gut::optional<SQLite::PreparedStatement> m_unmute_post;
  gut::optional<SQLite::PreparedStatement> m_mute_person;
  gut::optional<SQLite::PreparedStatement> m_unmute_person;
  gut::optional<SQLite::PreparedStatement> m_mute_culture;
  gut::optional<SQLite::PreparedStatement> m_unmute_culture;
  gut::optional<SQLite::PreparedStatement> m_new_post;
  gut::optional<SQLite::PreparedStatement> m_set_author;

  void session_views() const;
  void transient_views();
  void session_operations();
};
