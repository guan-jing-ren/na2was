#include "was-five-sql.hpp"

#include <chrono>
#include <iostream>

using namespace gut;
using namespace std;

namespace internal {
constexpr static reflect_enum reflect_SIGNUPS_Access =
    GUT_REFLECT_ENUM(Site::SIGNUPS::Access,
                     GUT_REFLECT_MEMBERS(BANNED, PENDING, REGISTERED, REMOVED));
constexpr static reflect_struct reflect_SIGNUPS = GUT_REFLECT_STRUCT(
    Site::SIGNUPS, GUT_REFLECT_MEMBERS(ID, EMAIL, ACCESS), GUT_REFLECT_BASES());
constexpr static reflect_enum reflect_PEOPLE_Membership = GUT_REFLECT_ENUM(
    Site::PEOPLE::Membership, GUT_REFLECT_MEMBERS(FREE, SUBSIDIZED, PAID));
constexpr static reflect_struct reflect_PEOPLE = GUT_REFLECT_STRUCT(
    Site::PEOPLE,
    GUT_REFLECT_MEMBERS(ID, HANDLE, AVATAR, CREDENTIAL, MEMBERSHIP),
    GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_CULTURES = GUT_REFLECT_STRUCT(
    Site::CULTURES, GUT_REFLECT_MEMBERS(ID, CULTURE, TAGLINE, MISSION),
    GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_POSTS = GUT_REFLECT_STRUCT(
    Site::POSTS, GUT_REFLECT_MEMBERS(ID, CREATED, MODIFIED, TITLE, CONTENT),
    GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_AUTHORS = GUT_REFLECT_STRUCT(
    Site::AUTHORS, GUT_REFLECT_MEMBERS(AUTHOR, POST), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_RESPONSES = GUT_REFLECT_STRUCT(
    Site::RESPONSES, GUT_REFLECT_MEMBERS(POST, RESPONSE), GUT_REFLECT_BASES());
constexpr static reflect_enum reflect_PARTICIPANTS_Standing = GUT_REFLECT_ENUM(
    Site::PARTICIPANTS::Standing,
    GUT_REFLECT_MEMBERS(BANNED, PARTICIPANT, MODERATOR, ORGANIZER));
constexpr static reflect_struct reflect_PARTICIPANTS = GUT_REFLECT_STRUCT(
    Site::PARTICIPANTS, GUT_REFLECT_MEMBERS(CULTURE, PARTICIPANT, STANDING),
    GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_PUBLISHED = GUT_REFLECT_STRUCT(
    Site::PUBLISHED, GUT_REFLECT_MEMBERS(CULTURE, POST), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_BLOCKED_CULTURES = GUT_REFLECT_STRUCT(
    Site::BLOCKED_CULTURES, GUT_REFLECT_MEMBERS(PERSON, CULTURE),
    GUT_REFLECT_BASES());
constexpr static reflect_enum reflect_RELATIONSHIPS_Relation = GUT_REFLECT_ENUM(
    Site::RELATIONSHIPS::Relation,
    GUT_REFLECT_MEMBERS(BLOCKED, CONTACT, COLLEAGUE, FRIEND, FAMILY, TRUSTED));
constexpr static reflect_struct reflect_RELATIONSHIPS = GUT_REFLECT_STRUCT(
    Site::RELATIONSHIPS, GUT_REFLECT_MEMBERS(PERSON1, PERSON2, RELATION),
    GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_MUTED_POSTS = GUT_REFLECT_STRUCT(
    Site::MUTED_POSTS, GUT_REFLECT_MEMBERS(PERSON, POST), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_MUTED_PEOPLE = GUT_REFLECT_STRUCT(
    Site::MUTED_PEOPLE, GUT_REFLECT_MEMBERS(PERSON1, PERSON2),
    GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_MUTED_CULTURES = GUT_REFLECT_STRUCT(
    Site::MUTED_CULTURES, GUT_REFLECT_MEMBERS(PERSON, CULTURE),
    GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_ChangeEmail = GUT_REFLECT_STRUCT(
    Site::ChangeEmail, GUT_REFLECT_MEMBERS(SID, EMAIL), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_ChangeAccess = GUT_REFLECT_STRUCT(
    Site::ChangeAccess, GUT_REFLECT_MEMBERS(SID, ACCESS), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_NewSignup = GUT_REFLECT_STRUCT(
    Site::NewSignup, GUT_REFLECT_MEMBERS(EMAIL), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_AddPerson = GUT_REFLECT_STRUCT(
    Site::AddPerson, GUT_REFLECT_MEMBERS(ID), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_PERPEOPLE = GUT_REFLECT_STRUCT(
    Session::PERPEOPLE, GUT_REFLECT_MEMBERS(PERSON), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_PERCULTURE = GUT_REFLECT_STRUCT(
    Session::PERCULTURE, GUT_REFLECT_MEMBERS(CULTURE), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_PERSTREAM = GUT_REFLECT_STRUCT(
    Session::PERSTREAM, GUT_REFLECT_MEMBERS(CONTENT), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_ChangeHandle = GUT_REFLECT_STRUCT(
    Session::ChangeHandle, GUT_REFLECT_MEMBERS(HANDLE), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_ChangeCredential =
    GUT_REFLECT_STRUCT(Session::ChangeCredential,
                       GUT_REFLECT_MEMBERS(CREDENTIAL), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_ChangeMembership =
    GUT_REFLECT_STRUCT(Session::ChangeMembership,
                       GUT_REFLECT_MEMBERS(MEMBERSHIP), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_CultureIdBase = GUT_REFLECT_STRUCT(
    Session::CultureIdBase, GUT_REFLECT_MEMBERS(CID), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_ChangeCulture =
    GUT_REFLECT_STRUCT(Session::ChangeCulture, GUT_REFLECT_MEMBERS(CULTURE),
                       GUT_REFLECT_BASES(Session::CultureIdBase));
constexpr static reflect_struct reflect_ChangeTagline =
    GUT_REFLECT_STRUCT(Session::ChangeTagline, GUT_REFLECT_MEMBERS(TAGLINE),
                       GUT_REFLECT_BASES(Session::CultureIdBase));
constexpr static reflect_struct reflect_ChangeMission =
    GUT_REFLECT_STRUCT(Session::ChangeMission, GUT_REFLECT_MEMBERS(MISSION),
                       GUT_REFLECT_BASES(Session::CultureIdBase));
constexpr static reflect_struct reflect_EndCulture =
    GUT_REFLECT_STRUCT(Session::EndCulture, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::CultureIdBase));
constexpr static reflect_struct reflect_PostBase = GUT_REFLECT_STRUCT(
    Session::PostBase, GUT_REFLECT_MEMBERS(PID), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_ModifyPost =
    GUT_REFLECT_STRUCT(Session::ModifyPost, GUT_REFLECT_MEMBERS(MODIFIED),
                       GUT_REFLECT_BASES(Session::PostBase));
constexpr static reflect_struct reflect_ModifyTitle =
    GUT_REFLECT_STRUCT(Session::ModifyTitle, GUT_REFLECT_MEMBERS(TITLE),
                       GUT_REFLECT_BASES(Session::ModifyPost));
constexpr static reflect_struct reflect_ModifyContent =
    GUT_REFLECT_STRUCT(Session::ModifyContent, GUT_REFLECT_MEMBERS(CONTENT),
                       GUT_REFLECT_BASES(Session::ModifyPost));
constexpr static reflect_struct reflect_RemovePost =
    GUT_REFLECT_STRUCT(Session::RemovePost, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::PostBase));
constexpr static reflect_struct reflect_AddAuthor =
    GUT_REFLECT_STRUCT(Session::AddAuthor, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Site::AUTHORS));
constexpr static reflect_struct reflect_RemoveAuthor =
    GUT_REFLECT_STRUCT(Session::RemoveAuthor, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Site::AUTHORS));
constexpr static reflect_struct reflect_AddResponse =
    GUT_REFLECT_STRUCT(Session::AddResponse, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Site::RESPONSES));
constexpr static reflect_struct reflect_ParticipantBase =
    GUT_REFLECT_STRUCT(Session::ParticipantBase, GUT_REFLECT_MEMBERS(CID, PID),
                       GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_AddParticipant =
    GUT_REFLECT_STRUCT(Session::AddParticipant, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::ParticipantBase));
constexpr static reflect_struct reflect_RemoveParticipant =
    GUT_REFLECT_STRUCT(Session::RemoveParticipant, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::ParticipantBase));
constexpr static reflect_struct reflect_ChangeStanding =
    GUT_REFLECT_STRUCT(Session::ChangeStanding, GUT_REFLECT_MEMBERS(STANDING),
                       GUT_REFLECT_BASES(Session::ParticipantBase));
constexpr static reflect_struct reflect_PublishPost =
    GUT_REFLECT_STRUCT(Session::PublishPost, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Site::PUBLISHED));
constexpr static reflect_struct reflect_UnpublishPost =
    GUT_REFLECT_STRUCT(Session::UnpublishPost, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Site::PUBLISHED));
constexpr static reflect_struct reflect_CultureBase = GUT_REFLECT_STRUCT(
    Session::CultureBase, GUT_REFLECT_MEMBERS(CULTURE), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_BlockCulture =
    GUT_REFLECT_STRUCT(Session::BlockCulture, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::CultureBase));
constexpr static reflect_struct reflect_UnblockCulture =
    GUT_REFLECT_STRUCT(Session::UnblockCulture, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::CultureBase));
constexpr static reflect_struct reflect_Person2Base = GUT_REFLECT_STRUCT(
    Session::Person2Base, GUT_REFLECT_MEMBERS(PERSON2), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_AddRelation =
    GUT_REFLECT_STRUCT(Session::AddRelation, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::Person2Base));
constexpr static reflect_struct reflect_RemoveRelation =
    GUT_REFLECT_STRUCT(Session::RemoveRelation, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::Person2Base));
constexpr static reflect_struct reflect_ChangeRelation =
    GUT_REFLECT_STRUCT(Session::ChangeRelation, GUT_REFLECT_MEMBERS(RELATION),
                       GUT_REFLECT_BASES(Session::Person2Base));
constexpr static reflect_struct reflect_MutePostBase = GUT_REFLECT_STRUCT(
    Session::MutePostBase, GUT_REFLECT_MEMBERS(POST), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_MutePost =
    GUT_REFLECT_STRUCT(Session::MutePost, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::MutePostBase));
constexpr static reflect_struct reflect_UnmutePost =
    GUT_REFLECT_STRUCT(Session::UnmutePost, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::MutePostBase));
constexpr static reflect_struct reflect_MutePerson =
    GUT_REFLECT_STRUCT(Session::MutePerson, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::Person2Base));
constexpr static reflect_struct reflect_UnmutePerson =
    GUT_REFLECT_STRUCT(Session::UnmutePerson, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::Person2Base));
constexpr static reflect_struct reflect_MuteCulture =
    GUT_REFLECT_STRUCT(Session::MuteCulture, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::CultureBase));
constexpr static reflect_struct reflect_UnmuteCulture =
    GUT_REFLECT_STRUCT(Session::UnmuteCulture, GUT_REFLECT_MEMBERS(),
                       GUT_REFLECT_BASES(Session::CultureBase));
constexpr static reflect_struct reflect_NewPost = GUT_REFLECT_STRUCT(
    Session::NewPost, GUT_REFLECT_MEMBERS(TITLE, CONTENT, CREATED),
    GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_ReadStream = GUT_REFLECT_STRUCT(
    Session::ReadStream, GUT_REFLECT_MEMBERS(), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_ReadAuthor = GUT_REFLECT_STRUCT(
    Session::ReadAuthor, GUT_REFLECT_MEMBERS(AUTHOR), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_ReadCulture = GUT_REFLECT_STRUCT(
    Session::ReadCulture, GUT_REFLECT_MEMBERS(CULTURE), GUT_REFLECT_BASES());
constexpr static reflect_struct reflect_PostContent = GUT_REFLECT_STRUCT(
    Session::PostContent, GUT_REFLECT_MEMBERS(CONTENT), GUT_REFLECT_BASES());
} // namespace internal

constexpr static reflect_namespace reflect_Site = GUT_REFLECT_NAMESPACE(
    ::internal,
    GUT_REFLECT_MEMBERS(
        reflect_SIGNUPS_Access, reflect_SIGNUPS, reflect_PEOPLE_Membership,
        reflect_PEOPLE, reflect_CULTURES, reflect_POSTS, reflect_AUTHORS,
        reflect_RESPONSES, reflect_PARTICIPANTS_Standing, reflect_PARTICIPANTS,
        reflect_PUBLISHED, reflect_BLOCKED_CULTURES,
        reflect_RELATIONSHIPS_Relation, reflect_RELATIONSHIPS,
        reflect_MUTED_POSTS, reflect_MUTED_PEOPLE, reflect_MUTED_CULTURES,
        reflect_ChangeEmail, reflect_ChangeAccess, reflect_NewSignup,
        reflect_AddPerson));

constexpr static reflect_namespace reflect_Session = GUT_REFLECT_NAMESPACE(
    ::internal,
    GUT_REFLECT_MEMBERS(
        reflect_PERPEOPLE, reflect_PERCULTURE, reflect_PERSTREAM,
        reflect_ChangeHandle, reflect_ChangeCredential,
        reflect_PEOPLE_Membership, reflect_ChangeMembership,
        reflect_CultureIdBase, reflect_ChangeCulture, reflect_ChangeTagline,
        reflect_ChangeMission, reflect_EndCulture, reflect_PostBase,
        reflect_ModifyPost, reflect_ModifyTitle, reflect_ModifyContent,
        reflect_RemovePost, reflect_AUTHORS, reflect_AddAuthor,
        reflect_RemoveAuthor, reflect_RESPONSES, reflect_AddResponse,
        reflect_ParticipantBase, reflect_AddParticipant,
        reflect_RemoveParticipant, reflect_PARTICIPANTS_Standing,
        reflect_ChangeStanding, reflect_PUBLISHED, reflect_PublishPost,
        reflect_UnpublishPost, reflect_CultureBase, reflect_BlockCulture,
        reflect_UnblockCulture, reflect_Person2Base,
        reflect_RELATIONSHIPS_Relation, reflect_AddRelation,
        reflect_RemoveRelation, reflect_ChangeRelation, reflect_MutePostBase,
        reflect_MutePost, reflect_UnmutePost, reflect_MutePerson,
        reflect_UnmutePerson, reflect_MuteCulture, reflect_UnmuteCulture,
        reflect_NewPost, reflect_ReadStream, reflect_ReadAuthor,
        reflect_ReadCulture, reflect_PostContent));

Site::Site(SQLite &&db) : m_db(move(db)) {
  site_tables();
  site_operations();
}

void Site::operator()(const ChangeEmail &operand) const {
  m_db(reflect_Site, m_change_email.value(), operand);
}

void Site::operator()(const ChangeAccess &operand) const {
  m_db(reflect_Site, m_change_access.value(), operand);
}

void Site::operator()(const NewSignup &operand) const {
  m_db(reflect_Site, m_new_signup.value(), operand);
}

void Site::operator()(const AddPerson &operand) const {
  m_db(reflect_Site, m_add_person.value(), operand);
}

void Site::site_tables() const {
  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS SIGNUPS (
    ID INTEGER PRIMARY KEY,
    EMAIL TEXT UNIQUE NOT NULL,
    ACCESS TEXT NOT NULL
        CHECK (ACCESS IN ('BANNED', 'PENDING', 'REGISTERED', 'REMOVED')));
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS PEOPLE (
    ID INTEGER PRIMARY KEY REFERENCES SIGNUPS (ID)
        ON DELETE CASCADE,
    HANDLE TEXT NOT NULL,
    AVATAR BLOB,
    CREDENTIAL BLOB NOT NULL,
    MEMBERSHIP TEXT NOT NULL
        CHECK (MEMBERSHIP IN ('FREE', 'SUBSIDIZED', 'PAID')));
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS CULTURES (
    ID INTEGER PRIMARY KEY,
    CULTURE TEXT NOT NULL,
    TAGLINE TEXT,
    MISSION TEXT);
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS POSTS (
    ID INTEGER PRIMARY KEY,
    CREATED INTEGER NOT NULL,
    MODIFIED INTEGER NOT NULL,
    TITLE TEXT,
    CONTENT TEXT NOT NULL);
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS AUTHORS (
    AUTHOR REFERENCES PERSON (ID)
        ON DELETE CASCADE,
    POST REFERENCES POSTS (ID)
        ON DELETE CASCADE,
    PRIMARY KEY (AUTHOR, POST))
    WITHOUT ROWID;
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS RESPONSES (
    POST REFERENCES POSTS (ID)
        ON DELETE CASCADE,
    RESPONSE REFERENCES POSTS (ID)
        ON DELETE CASCADE,
    PRIMARY KEY (POST, RESPONSE))
    WITHOUT ROWID;
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS PARTICIPANTS (
    CULTURE REFERENCES CULTURES (ID)
        ON DELETE CASCADE,
    PARTICIPANT REFERENCES PEOPLE (ID)
        ON DELETE CASCADE,
    STANDING TEXT NOT NULL
        CHECK (STANDING IN ('BANNED', 'PARTICIPANT', 'MODERATOR', 'ORGANIZER')),
    PRIMARY KEY (CULTURE, PARTICIPANT, STANDING))
    WITHOUT ROWID;
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS PUBLISHED (
    CULTURE REFERENCES CULTURES (ID)
        ON DELETE CASCADE,
    POST REFERENCES POSTS (ID)
        ON DELETE CASCADE,
    PRIMARY KEY (CULTURE, POST))
    WITHOUT ROWID;
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS BLOCKED_CULTURES (
    PERSON REFERENCES PEOPLE (ID)
        ON DELETE CASCADE,
    CULTURE REFERENCES CULTURES (ID)
        ON DELETE CASCADE,
    PRIMARY KEY (PERSON, CULTURE))
    WITHOUT ROWID;
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS RELATIONSHIPS (
    PERSON1 REFERENCES PEOPLE (ID)
        ON DELETE CASCADE,
    PERSON2 REFERENCES PEOPLE (ID)
        ON DELETE CASCADE,
    RELATION TEXT NOT NULL
        CHECK (RELATION IN ('BLOCKED', 'CONTACT', 'COLLEAGUE', 'FRIEND', 'FAMILY', 'TRUSTED')),
    PRIMARY KEY (PERSON1, PERSON2, RELATION))
    WITHOUT ROWID;
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS MUTED_POSTS (
    PERSON REFERENCES PEOPLE (ID)
        ON DELETE CASCADE,
    POST REFERENCES POSTS (ID)
        ON DELETE CASCADE,
    PRIMARY KEY (PERSON, POST))
    WITHOUT ROWID;
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS MUTED_PEOPLE (
    PERSON1 REFERENCES PEOPLE (ID)
        ON DELETE CASCADE,
    PERSON2 REFERENCES PEOPLE (ID)
        ON DELETE CASCADE,
    PRIMARY KEY (PERSON1, PERSON2))
    WITHOUT ROWID;
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TABLE IF NOT EXISTS MUTED_CULTURES (
    PERSON REFERENCES PEOPLE (ID)
        ON DELETE CASCADE,
    CULTURE REFERENCES CULTURES (ID)
        ON DELETE CASCADE,
    PRIMARY KEY (PERSON, CULTURE))
    WITHOUT ROWID;
)SQL"}();
}

void Site::site_operations() {
  m_change_email = {m_db, R"SQL(
UPDATE SIGNUPS SET EMAIL = @EMAIL WHERE SIGNUPS.ID = @SID;
)SQL"};

  m_change_access = {m_db, R"SQL(
UPDATE SIGNUPS SET ACCESS = @ACCESS WHERE SIGNUPS.ID = @SID;
)SQL"};

  m_new_signup = {m_db, R"SQL(
INSERT INTO SIGNUPS (ID, EMAIL, ACCESS) VALUES (NULL, @EMAIL, 'PENDING');
)SQL"};

  m_add_person = {m_db, R"SQL(
INSERT INTO PEOPLE (ID, HANDLE, AVATAR, CREDENTIAL, MEMBERSHIP) VALUES (@ID, (SELECT SIGNUPS.EMAIL FROM SIGNUPS WHERE SIGNUPS.ID = @ID), NULL, UPPER(HEX(RANDOMBLOB(24))), 'FREE');
)SQL"};
}

Session::Session(SQLite &&db, int64_t person_id)
    : m_db(move(db)), m_person_id(person_id) {
  session_views();
  transient_views();
  session_operations();
}

void Session::session_views() const {
  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TEMPORARY TABLE IF NOT EXISTS SESSION_PERSON (
    PERSON REFERENCES PEOPLE (ID)
        ON DELETE CASCADE,
    SESSID BLOB NOT NULL,
    PERSON_FILTER REFERENCES PEOPLE (ID)
        ON DELETE SET NULL,
    CULTURE_FILTER REFERENCES CULTURES (ID)
        ON DELETE SET NULL,
    PRIMARY KEY (PERSON));
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
INSERT OR REPLACE INTO SESSION_PERSON (PERSON, SESSID) VALUES (@PERSON, UPPER(HEX(RANDOMBLOB(16))));
)SQL"}({{{"@PERSON", m_person_id}}});

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TEMPORARY VIEW IF NOT EXISTS PERPEOPLE AS
WITH
    CURRENT_PERSON AS (
        SELECT SESSION_PERSON.PERSON AS PERSON FROM SESSION_PERSON),

    PEOPLE_RELATED AS (
        SELECT RELATIONSHIPS.PERSON2 AS PERSON FROM RELATIONSHIPS
            INNER JOIN CURRENT_PERSON ON (RELATIONSHIPS.PERSON1 = CURRENT_PERSON.PERSON)
        WHERE RELATIONSHIPS.RELATION <> 'BLOCKED'),

    PEOPLE_MUTED AS (
        SELECT MUTED_PEOPLE.PERSON2 AS PERSON FROM MUTED_PEOPLE
            INNER JOIN CURRENT_PERSON ON (MUTED_PEOPLE.PERSON1 = CURRENT_PERSON.PERSON)),

    PEOPLE_BLOCKED_BY AS (
        SELECT RELATIONSHIPS.PERSON1 AS PERSON FROM RELATIONSHIPS
            INNER JOIN CURRENT_PERSON ON (RELATIONSHIPS.PERSON2 = CURRENT_PERSON.PERSON)
        WHERE RELATIONSHIPS.RELATION = 'BLOCKED'),

    PEOPLE_INVISIBLE AS (
        SELECT PEOPLE_MUTED.PERSON AS PERSON FROM PEOPLE_MUTED
        UNION
        SELECT PEOPLE_BLOCKED_BY.PERSON AS PERSON FROM PEOPLE_BLOCKED_BY
        UNION
        SELECT SIGNUPS.ID AS PERSON FROM SIGNUPS WHERE SIGNUPS.ACCESS <> 'REGISTERED'),

    PEOPLE_VISIBLE AS (
        SELECT PEOPLE_RELATED.PERSON AS PERSON FROM PEOPLE_RELATED
        UNION
        SELECT CURRENT_PERSON.PERSON AS PERSON FROM CURRENT_PERSON
        EXCEPT
        SELECT PEOPLE_INVISIBLE.PERSON AS PERSON FROM PEOPLE_INVISIBLE)
SELECT PEOPLE_VISIBLE.PERSON AS PERSON FROM PEOPLE_VISIBLE;
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TEMPORARY VIEW IF NOT EXISTS PERCULTURE AS
WITH
    CURRENT_PERSON AS (
        SELECT SESSION_PERSON.PERSON AS PERSON FROM SESSION_PERSON),

    CULTURES_PARTICIPATING AS (
        SELECT PARTICIPANTS.CULTURE AS CULTURE FROM PARTICIPANTS
            INNER JOIN CURRENT_PERSON ON (PARTICIPANTS.PARTICIPANT = CURRENT_PERSON.PERSON)
        WHERE PARTICIPANTS.STANDING <> 'BANNED'),

    CULTURES_MUTED AS (
        SELECT MUTED_CULTURES.CULTURE AS CULTURE FROM MUTED_CULTURES
            INNER JOIN CURRENT_PERSON ON (MUTED_CULTURES.PERSON = CURRENT_PERSON.PERSON)),

    CULTURES_BLOCKED AS (
        SELECT BLOCKED_CULTURES.CULTURE AS CULTURE FROM BLOCKED_CULTURES
            INNER JOIN CURRENT_PERSON ON (BLOCKED_CULTURES.PERSON = CURRENT_PERSON.PERSON)),

    CULTURES_INVISIBLE AS (
        SELECT CULTURES_MUTED.CULTURE AS CULTURE FROM CULTURES_MUTED
        UNION
        SELECT CULTURES_BLOCKED.CULTURE AS CULTURE FROM CULTURES_BLOCKED),

    CULTURES_VISIBLE AS (
        SELECT CULTURES_PARTICIPATING.CULTURE AS CULTURE FROM CULTURES_PARTICIPATING
        EXCEPT
        SELECT CULTURES_INVISIBLE.CULTURE AS CULTURE FROM CULTURES_INVISIBLE)
SELECT CULTURES_VISIBLE.CULTURE AS CULTURE FROM CULTURES_VISIBLE;
)SQL"}();

  SQLite::PreparedStatement{m_db, R"SQL(
CREATE TEMPORARY VIEW IF NOT EXISTS PERSTREAM AS
WITH
    CURRENT_PERSON AS (
        SELECT SESSION_PERSON.PERSON AS PERSON FROM SESSION_PERSON),

    POSTS_SUBSCRIBED AS (
        SELECT AUTHORS.POST AS POST FROM AUTHORS
            INNER JOIN PERPEOPLE ON (AUTHORS.AUTHOR = PERPEOPLE.PERSON)
        UNION
        SELECT PUBLISHED.POST AS POST FROM PUBLISHED
            INNER JOIN PERCULTURE ON (PUBLISHED.CULTURE = PERCULTURE.CULTURE)),

    POSTS_MUTED AS (
        SELECT MUTED_POSTS.POST AS POST FROM MUTED_POSTS
            INNER JOIN CURRENT_PERSON ON (MUTED_POSTS.PERSON = CURRENT_PERSON.PERSON)),

    POSTS_VISIBLE AS (
        SELECT DISTINCT POSTS_SUBSCRIBED.POST AS POST FROM POSTS_SUBSCRIBED
        EXCEPT
        SELECT POSTS_MUTED.POST AS POST FROM POSTS_MUTED)
SELECT POSTS.CONTENT AS CONTENT FROM POSTS
    INNER JOIN POSTS_VISIBLE ON (POSTS.ID = POSTS_VISIBLE.POST)
ORDER BY POSTS.CREATED DESC;
)SQL"}();
}

void Session::transient_views() {
  m_post_by_author = {m_db, R"SQL(
WITH
    PERSON_VISIBLE AS (
    SELECT PERPEOPLE.PERSON AS PERSON FROM PERPEOPLE
    WHERE PERPEOPLE.PERSON = @AUTHOR),

    POSTS_AUTHOR AS (
    SELECT AUTHORS.POST AS POST FROM AUTHORS
        INNER JOIN PERSON_VISIBLE ON (AUTHORS.AUTHOR = PERSON_VISIBLE.PERSON))
SELECT POSTS.CONTENT AS CONTENT FROM POSTS
    INNER JOIN POSTS_AUTHOR ON (POSTS.ID = POSTS_AUTHOR.POST)
ORDER BY POSTS.CREATED DESC;
)SQL"};

  m_publish_by_culture = {m_db, R"SQL(
WITH
    CULTURE_VISIBLE AS (
    SELECT PERCULTURE.CULTURE AS CULTURE FROM PERCULTURE
    WHERE PERCULTURE.CULTURE = @CULTURE),

    PUBLISHED_POSTS AS (
    SELECT PUBLISHED.POST AS POST FROM PUBLISHED
        INNER JOIN CULTURE_VISIBLE ON (PUBLISHED.CULTURE = CULTURE_VISIBLE.CULTURE))
SELECT POSTS.CONTENT AS CONTENT FROM POSTS
    INNER JOIN PUBLISHED_POSTS ON (POSTS.ID = PUBLISHED_POSTS.POST)
ORDER BY POSTS.CREATED DESC;
)SQL"};

  m_post_stream = {m_db, R"SQL(
SELECT PERSTREAM.CONTENT FROM PERSTREAM;
)SQL"};
}

void Session::session_operations() {
  m_change_handle = {m_db, R"SQL(
UPDATE PEOPLE SET HANDLE = @HANDLE WHERE PEOPLE.ID IN (SELECT SESSION_PERSON.PERSON AS ID FROM SESSION_PERSON);
)SQL"};

  m_change_credential = {m_db, R"SQL(
UPDATE PEOPLE SET CREDENTIAL = @CREDENTIAL WHERE PEOPLE.ID IN (SELECT SESSION_PERSON.PERSON AS ID FROM SESSION_PERSON);
)SQL"};

  m_change_membership = {m_db, R"SQL(
UPDATE PEOPLE SET MEMBERSHIP = @MEMBERSHIP WHERE PEOPLE.ID IN (SELECT SESSION_PERSON.PERSON AS ID FROM SESSION_PERSON);
)SQL"};

  m_change_culture = {m_db, R"SQL(
UPDATE CULTURES SET CULTURE = @CULTURE WHERE CULTURES.ID = @CID;
)SQL"};

  m_change_tagline = {m_db, R"SQL(
UPDATE CULTURES SET TAGLINE = @TAGLINE WHERE CULTURES.ID = @CID;
)SQL"};

  m_change_mission = {m_db, R"SQL(
UPDATE CULTURES SET MISSION = @MISSION WHERE CULTURES.ID = @CID;
)SQL"};

  m_end_culture = {m_db, R"SQL(
DELETE FROM CULTURES WHERE CULTURES.ID = @CID;
)SQL"};

  m_modify_title = {m_db, R"SQL(
UPDATE POSTS SET MODIFIED = @MODIFIED, TITLE = @TITLE WHERE POSTS.ID = @PID;
)SQL"};

  m_modify_content = {m_db, R"SQL(
UPDATE POSTS SET MODIFIED = @MODIFIED, CONTENT = @CONTENT WHERE POSTS.ID = @PID;
)SQL"};

  m_remove_post = {m_db, R"SQL(
DELETE FROM POSTS WHERE POSTS.ID = @PID;
)SQL"};

  m_add_author = {m_db, R"SQL(
INSERT INTO AUTHORS (AUTHOR, POST) VALUES (@AUTHOR, @POST);
)SQL"};

  m_remove_author = {m_db, R"SQL(
DELETE FROM AUTHORS WHERE AUTHORS.AUTHOR = @AUTHOR AND AUTHORS.POST = @POST;
)SQL"};

  m_add_response = {m_db, R"SQL(
INSERT INTO RESPONSES (POST, RESPONSE) VALUES (@POST, @RESPONSE);
)SQL"};

  m_add_participant = {m_db, R"SQL(
INSERT INTO PARTICIPANTS (CULTURE, PARTICIPANT, STANDING) VALUES (@CID, @PID, 'PARTICIPANT');
)SQL"};

  m_remove_participant = {m_db, R"SQL(
DELETE FROM PARTICIPANTS WHERE PARTICIPANTS.CULTURE = @CID AND PARTICIPANTS.PARTICIPANT = @PID;
)SQL"};

  m_change_standing = {m_db, R"SQL(
UPDATE PARTICIPANTS SET STANDING = @STANDING WHERE PARTICIPANTS.CULTURE = @CID AND PARTICIPANTS.PARTICIPANT = @PID;
)SQL"};

  m_publish_post = {m_db, R"SQL(
INSERT INTO PUBLISHED (CULTURE, POST) VALUES (@CULTURE, @POST);
)SQL"};

  m_unpublish_post = {m_db, R"SQL(
DELETE FROM PUBLISHED WHERE PUBLISHED.CULTURE = @CULTURE AND PUBLISHED.POST = @POST;
)SQL"};

  m_block_culture = {m_db, R"SQL(
INSERT INTO BLOCKED_CULTURES (PERSON, CULTURE) VALUES ((SELECT SESSION_PERSON.PERSON AS PERSON FROM SESSION_PERSON), @CULTURE);
)SQL"};

  m_unblock_culture = {m_db, R"SQL(
DELETE FROM BLOCKED_CULTURES WHERE BLOCKED_CULTURES.PERSON IN (SELECT SESSION_PERSON.PERSON AS PERSON FROM SESSION_PERSON) AND BLOCKED_CULTURES.CULTURE = @CULTURE;
)SQL"};

  m_add_relation = {m_db, R"SQL(
INSERT INTO RELATIONSHIPS (PERSON1, PERSON2, RELATION) VALUES ((SELECT SESSION_PERSON.PERSON AS PERSON1 FROM SESSION_PERSON), @PERSON2, 'CONTACT');
)SQL"};

  m_remove_relation = {m_db, R"SQL(
DELETE FROM RELATIONSHIPS WHERE RELATIONSHIPS.PERSON1 IN (SELECT SESSION_PERSON.PERSON AS PERSON1 FROM SESSION_PERSON) AND RELATIONSHIPS.PERSON2 = @PERSON2;
)SQL"};

  m_change_relation = {m_db, R"SQL(
UPDATE RELATIONSHIPS SET RELATION = @RELATION WHERE RELATIONSHIPS.PERSON1 IN (SELECT SESSION_PERSON.PERSON AS PERSON1 FROM SESSION_PERSON) AND RELATIONSHIPS.PERSON2 = @PERSON2;
)SQL"};

  m_mute_post = {m_db, R"SQL(
INSERT INTO MUTED_POSTS (PERSON, POST) VALUES ((SELECT SESSION_PERSON.PERSON AS PERSON FROM SESSION_PERSON), @POST);
)SQL"};

  m_unmute_post = {m_db, R"SQL(
DELETE FROM MUTED_POSTS WHERE MUTED_POSTS.PERSON IN (SELECT SESSION_PERSON.PERSON AS PERSON FROM SESSION_PERSON) AND MUTED_POSTS.POST = @POST;
)SQL"};

  m_mute_person = {m_db, R"SQL(
INSERT INTO MUTED_PEOPLE (PERSON1, PERSON2) VALUES ((SELECT SESSION_PERSON.PERSON AS PERSON1 FROM SESSION_PERSON), @PERSON2);
)SQL"};

  m_unmute_person = {m_db, R"SQL(
DELETE FROM MUTED_PEOPLE WHERE MUTED_PEOPLE.PERSON1 IN (SELECT SESSION_PERSON.PERSON AS PERSON1 FROM SESSION_PERSON) AND MUTED_PEOPLE.PERSON2 = @PERSON2;
)SQL"};

  m_mute_culture = {m_db, R"SQL(
INSERT INTO MUTED_CULTURES (PERSON, CULTURE) VALUES ((SELECT SESSION_PERSON.PERSON AS PERSON FROM SESSION_PERSON), @CULTURE);
)SQL"};

  m_unmute_culture = {m_db, R"SQL(
DELETE FROM MUTED_CULTURES WHERE MUTED_CULTURES.PERSON IN (SELECT SESSION_PERSON.PERSON AS PERSON FROM SESSION_PERSON) AND MUTED_CULTURES.CULTURE = @CULTURE;
)SQL"};

  m_new_post = {m_db, R"SQL(
INSERT INTO POSTS (ID, CREATED, MODIFIED, TITLE, CONTENT) VALUES (NULL, @CREATED, @CREATED, @TITLE, @CONTENT);
)SQL"};

  m_set_author = {m_db, R"SQL(
INSERT INTO AUTHORS (AUTHOR, POST) VALUES ((SELECT SESSION_PERSON.PERSON AS PERSON FROM SESSION_PERSON), last_insert_rowid());
)SQL"};
}

void Session::operator()(const ChangeHandle &operation) const {
  m_db(reflect_Session, m_change_handle.value(), operation);
}

void Session::operator()(const ChangeCredential &operation) const {
  m_db(reflect_Session, m_change_credential.value(), operation);
}

void Session::operator()(const ChangeMembership &operation) const {
  m_db(reflect_Session, m_change_membership.value(), operation);
}

void Session::operator()(const ChangeCulture &operation) const {
  m_db(reflect_Session, m_change_culture.value(), operation);
}

void Session::operator()(const ChangeTagline &operation) const {
  m_db(reflect_Session, m_change_tagline.value(), operation);
}

void Session::operator()(const ChangeMission &operation) const {
  m_db(reflect_Session, m_change_mission.value(), operation);
}

void Session::operator()(const EndCulture &operation) const {
  m_db(reflect_Session, m_end_culture.value(), operation);
}

void Session::operator()(const ModifyTitle &operation) const {
  m_db(reflect_Session, m_modify_title.value(), operation);
}

void Session::operator()(const ModifyContent &operation) const {
  m_db(reflect_Session, m_modify_content.value(), operation);
}

void Session::operator()(const RemovePost &operation) const {
  m_db(reflect_Session, m_remove_post.value(), operation);
}

void Session::operator()(const AddAuthor &operation) const {
  m_db(reflect_Session, m_add_author.value(), operation);
}

void Session::operator()(const RemoveAuthor &operation) const {
  m_db(reflect_Session, m_remove_author.value(), operation);
}

void Session::operator()(const AddResponse &operation) const {
  m_db(reflect_Session, m_add_response.value(), operation);
}

void Session::operator()(const AddParticipant &operation) const {
  m_db(reflect_Session, m_add_participant.value(), operation);
}

void Session::operator()(const RemoveParticipant &operation) const {
  m_db(reflect_Session, m_remove_participant.value(), operation);
}

void Session::operator()(const ChangeStanding &operation) const {
  m_db(reflect_Session, m_change_standing.value(), operation);
}

void Session::operator()(const PublishPost &operation) const {
  m_db(reflect_Session, m_publish_post.value(), operation);
}

void Session::operator()(const UnpublishPost &operation) const {
  m_db(reflect_Session, m_unpublish_post.value(), operation);
}

void Session::operator()(const BlockCulture &operation) const {
  m_db(reflect_Session, m_block_culture.value(), operation);
}

void Session::operator()(const UnblockCulture &operation) const {
  m_db(reflect_Session, m_unblock_culture.value(), operation);
}

void Session::operator()(const AddRelation &operation) const {
  m_db(reflect_Session, m_add_relation.value(), operation);
}

void Session::operator()(const RemoveRelation &operation) const {
  m_db(reflect_Session, m_remove_relation.value(), operation);
}

void Session::operator()(const ChangeRelation &operation) const {
  m_db(reflect_Session, m_change_relation.value(), operation);
}

void Session::operator()(const MutePost &operation) const {
  m_db(reflect_Session, m_mute_post.value(), operation);
}

void Session::operator()(const UnmutePost &operation) const {
  m_db(reflect_Session, m_unmute_post.value(), operation);
}

void Session::operator()(const MutePerson &operation) const {
  m_db(reflect_Session, m_mute_person.value(), operation);
}

void Session::operator()(const UnmutePerson &operation) const {
  m_db(reflect_Session, m_unmute_person.value(), operation);
}

void Session::operator()(const MuteCulture &operation) const {
  m_db(reflect_Session, m_mute_culture.value(), operation);
}

void Session::operator()(const UnmuteCulture &operation) const {
  m_db(reflect_Session, m_unmute_culture.value(), operation);
}

void Session::operator()(const NewPost &operation) const {
  m_db.transact([this, &operation](auto &&) {
    m_db(reflect_Session, m_new_post.value(), operation);
    m_set_author.value()();
  });
}

auto Session::operator()(const ReadStream &operation) const
    -> std::vector<PostContent> {
  std::vector<PostContent> contents;
  m_db(reflect_Session, m_post_stream.value(), operation,
       gut::optional{gut::ref{contents}});
  return contents;
}

auto Session::operator()(const ReadAuthor &operation) const
    -> std::vector<PostContent> {
  std::vector<PostContent> contents;
  m_db(reflect_Session, m_post_by_author.value(), operation,
       gut::optional{gut::ref{contents}});
  return contents;
}

auto Session::operator()(const ReadCulture &operation) const
    -> std::vector<PostContent> {
  std::vector<PostContent> contents;
  m_db(reflect_Session, m_publish_by_culture.value(), operation,
       gut::optional{gut::ref{contents}});
  return contents;
}
